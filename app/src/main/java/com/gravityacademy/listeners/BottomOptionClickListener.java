package com.gravityacademy.listeners;


public interface BottomOptionClickListener {

    void onWorkoutButtonClicked();

    void onFavouriteButtonClicked();

    void onTimelineButtonClicked();

    void onAcademyButtonClicked();

    void onProfileButtonClicked();
}