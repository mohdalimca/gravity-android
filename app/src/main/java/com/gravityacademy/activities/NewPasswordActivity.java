package com.gravityacademy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.response.CommonResponse;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;
import com.gravityacademy.utils.ViewUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class NewPasswordActivity extends AppCompatActivity implements View.OnClickListener {


    private Button btnSave;
    private EditText etPassword;
    private EditText etOtp;
    private TextView tvLogin;
    private LinearLayout layoutProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        initView();
    }

    private void initView() {
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        btnSave = findViewById(R.id.btnSave);
        etOtp = findViewById(R.id.etOtp);
        etPassword = findViewById(R.id.etPassword);
        tvLogin = findViewById(R.id.tvLogin);

        btnSave.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnSave:
                if (!AppUtils.isValidateString(NewPasswordActivity.this, etOtp)) {
                    showSnackBar(getString(R.string.otp_is_empty));
                } else if (!AppUtils.isValidateString(NewPasswordActivity.this, etPassword)) {
                    showSnackBar(getString(R.string.password_is_invalid));
                } else {
                    ///Api CAll
                    callValidateOtpAPI();
                }
                break;

            case R.id.tvLogin:
                startActivity(new Intent(NewPasswordActivity.this, LoginActivity.class));
                finishAffinity();
                break;
        }

    }


    private void showSnackBar(String errorMessage) {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinatorLayout);
        final Snackbar snackbar = ViewUtils.showCustomSnackBar(NewPasswordActivity.this, coordinatorLayout, errorMessage);
        snackbar.show();
    }

    private void callValidateOtpAPI(){
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().validateOTP(etOtp.getText().toString()
        ).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!NewPasswordActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponse_code() == SUCCESS_RESPONSE_CODE) {
                            Utility.showToastString(NewPasswordActivity.this, response.body().getMessage());
                            callChangePasswordAPI();
                        } else {
                            Utility.showToast(NewPasswordActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null){
                        Utility.showToastString(NewPasswordActivity.this, AppUtils.getError(response));
                    }else {
                        Utility.showToast(NewPasswordActivity.this, R.string.something_went_wrong);
                    }
                }
            }
            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!NewPasswordActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(NewPasswordActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }
    private void callChangePasswordAPI(){
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().changePassword(etPassword.getText().toString(),etOtp.getText().toString()
        ).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!NewPasswordActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponse_code() == SUCCESS_RESPONSE_CODE) {
                            Utility.showToastString(NewPasswordActivity.this, response.body().getMessage());
                            startActivity(new Intent(NewPasswordActivity.this, HomeActivity.class));
                            finishAffinity();
                        } else {
                            Utility.showToast(NewPasswordActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null){
                        Utility.showToastString(NewPasswordActivity.this, AppUtils.getError(response));
                    }else {
                        Utility.showToast(NewPasswordActivity.this, R.string.something_went_wrong);
                    }
                }
            }
            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!NewPasswordActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(NewPasswordActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }
}
