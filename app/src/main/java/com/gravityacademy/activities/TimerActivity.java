package com.gravityacademy.activities;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;

public class TimerActivity extends AppCompatActivity implements View.OnClickListener {

    Chronometer simpleChronometer;
    private TextView tvPlay;
    private ImageView ivPlay;
    private ImageView ivClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        init();
    }

    private void init() {
        simpleChronometer = findViewById(R.id.chronometer);
        findViewById(R.id.viewStart).setOnClickListener(this);
        tvPlay = findViewById(R.id.tvPlay);
        findViewById(R.id.viewReset).setOnClickListener(this);
        ivPlay = findViewById(R.id.ivPlay);
        findViewById(R.id.ivClose).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.ivClose:
                onBackPressed();
                break;
            case R.id.viewReset:
                simpleChronometer.setBase(SystemClock.elapsedRealtime());

                break;

            case R.id.viewStart:
                if (tvPlay.getText().toString().equals(getString(R.string.start))) {
                    simpleChronometer.start();
                    tvPlay.setText(R.string.pause);
                    ivPlay.setImageResource(R.mipmap.pause);
                } else {
                    simpleChronometer.stop();
                    tvPlay.setText(R.string.start);
                    ivPlay.setImageResource(R.mipmap.play);
                }
                break;
        }
    }
}
