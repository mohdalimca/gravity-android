package com.gravityacademy.activities;

import android.content.Intent;
import android.media.Image;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.ViewUtils;
import com.super_rabbit.wheel_picker.WheelPicker;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.KEY_SIGN_UP_DATA;

public class FitnessLevelActivity extends AppCompatActivity implements View.OnClickListener {

    private CoordinatorLayout coordinatorLayout;

    private LinearLayout newbie_layout;
    private LinearLayout beginner_layout;
    private LinearLayout intermediate_layout;
    private LinearLayout advanced_layout;

    private TextView newbie_tv;
    private TextView beginner_tv;
    private TextView intermediate_tv;
    private TextView advanced_tv;

    private TextView i_never_tv;
    private TextView some_exp_tv;
    private TextView moderate_exp_tv;
    private TextView very_exp_tv;
    ProgressBar progress;

    ImageView ivClose;
    Button btnSave;
    Integer count;
    private String selectedStr;
    private SignUpData signUpData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness_level);
        initView();
        getBundleData();
        fromCreateAccount();
        getSignUpData();
    }
    private void getSignUpData() {
        if (getIntent() != null && getIntent().getSerializableExtra(KEY_SIGN_UP_DATA) != null) {
            signUpData = (SignUpData) getIntent().getSerializableExtra(KEY_SIGN_UP_DATA);
        }
    }
    private void getBundleData(){
        if (getIntent() != null && getIntent().getStringExtra(AppConstant.FITNESS_LEVEL) != null) {
            progress.setVisibility(View.GONE);
            String str = getIntent().getStringExtra(AppConstant.FITNESS_LEVEL);
            if (str.equalsIgnoreCase(getString(R.string.newbie))){
                count = 0;
                setOrangeLayout(count);
            }else if (str.equalsIgnoreCase(getString(R.string.beginner))){
                count = 1;
                setOrangeLayout(count);
            }else if (str.equalsIgnoreCase(getString(R.string.intermediate))){
                count = 2;
                setOrangeLayout(count);
            }else if (str.equalsIgnoreCase(getString(R.string.advanced))){
                count = 3;
                setOrangeLayout(count);
            }
            ivClose.setImageResource(R.mipmap.close);
        }
    }

    private void initView(){
        progress = findViewById(R.id.progress);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        btnSave = findViewById(R.id.btnSave);
        ivClose = findViewById(R.id.ivClose);


        newbie_layout = findViewById(R.id.newbie_layout);
        beginner_layout = findViewById(R.id.beginner_layout);
        intermediate_layout = findViewById(R.id.intermediate_layout);
        advanced_layout = findViewById(R.id.advanced_layout);

        newbie_tv = findViewById(R.id.newbie_tv);
        beginner_tv = findViewById(R.id.beginner_tv);
        intermediate_tv = findViewById(R.id.intermediate_tv);
        advanced_tv = findViewById(R.id.advanced_tv);

        i_never_tv = findViewById(R.id.i_never_tv);
        some_exp_tv = findViewById(R.id.some_exp_tv);
        moderate_exp_tv = findViewById(R.id.moderate_exp_tv);
        very_exp_tv = findViewById(R.id.very_exp_tv);

        ivClose.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        newbie_layout.setOnClickListener(this);
        beginner_layout.setOnClickListener(this);
        intermediate_layout.setOnClickListener(this);
        advanced_layout.setOnClickListener(this);
    }

    private void fromCreateAccount(){
        ProgressBar progress = findViewById(R.id.progress);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            boolean isFromCreateAc = bundle.getBoolean(KEY_IS_FROM_CREATE_AC);
            if (isFromCreateAc){
                ivClose.setImageResource(R.mipmap.back);
                progress.setVisibility(View.VISIBLE);
                btnSave.setText(getString(R.string.next));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.newbie_layout:
                selectedStr = newbie_tv.getText().toString();
                count = 0;
                setOrangeLayout(count);
                break;
            case R.id.beginner_layout:
                selectedStr = beginner_tv.getText().toString();
                count = 1;
                setOrangeLayout(count);
                break;
            case R.id.intermediate_layout:
                selectedStr = intermediate_tv.getText().toString();
                count = 2;
                setOrangeLayout(count);
                break;
            case R.id.advanced_layout:
                selectedStr = advanced_tv.getText().toString();
                count = 3;
                setOrangeLayout(count);
                break;
            case R.id.btnSave:
                if (btnSave.getText().toString().equalsIgnoreCase(getString(R.string.save))) {
                    Intent intent = new Intent();
                    intent.putExtra(AppConstant.FITNESS_LEVEL, selectedStr);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    if (count == null){
                        Snackbar snackbar = ViewUtils.showCustomSnackBar(this,coordinatorLayout, getString(R.string.please_choose_a_fitness_level));
                        snackbar.show();
                        return;
                    }
                    signUpData.setFn_level_id(count+1);
                    Intent intent = new Intent(this, GoalsActivity.class);
                    intent.putExtra(KEY_IS_FROM_CREATE_AC, true);
                    intent.putExtra(KEY_SIGN_UP_DATA, signUpData);
                    startActivity(intent);
                }
                break;
        }
    }

    private void setOrangeLayout(int count){
        setWhiteLayout();
        if (count == 0){
            newbie_layout.setBackgroundResource(R.drawable.rounded_orange_border_background);
            newbie_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
            i_never_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }else if (count == 1){
            beginner_layout.setBackgroundResource(R.drawable.rounded_orange_border_background);
            beginner_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
            some_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }else if (count == 2){
            intermediate_layout.setBackgroundResource(R.drawable.rounded_orange_border_background);
            intermediate_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
            moderate_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }else if (count == 3){
            advanced_layout.setBackgroundResource(R.drawable.rounded_orange_border_background);
            advanced_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
            very_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }
    }

    private void setWhiteLayout(){
        newbie_layout.setBackgroundResource(R.drawable.rounded_border_background);
        newbie_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        i_never_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        beginner_layout.setBackgroundResource(R.drawable.rounded_border_background);
        beginner_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        some_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        intermediate_layout.setBackgroundResource(R.drawable.rounded_border_background);
        intermediate_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        moderate_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        advanced_layout.setBackgroundResource(R.drawable.rounded_border_background);
        advanced_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        very_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));

    }

}
