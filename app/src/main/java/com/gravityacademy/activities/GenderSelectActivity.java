package com.gravityacademy.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.ViewUtils;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.KEY_SIGN_UP_DATA;

public class GenderSelectActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivClose;
    private ImageView ivMale;
    private ImageView ivFemale;
    private FrameLayout layoutMale;
    private FrameLayout layoutFemale;
    private TextView tvMale;
    private TextView tvFemale;
    private AppCompatButton btnSave;
    private Boolean isFemaleSelected;
    private ProgressBar progress;
    private SignUpData signUpData;

    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender_select);
        signUpData = new SignUpData();
        progress = findViewById(R.id.progress);
        ivClose = findViewById(R.id.ivClose);
        layoutMale = findViewById(R.id.layoutMale);
        layoutFemale = findViewById(R.id.layoutFemale);
        tvMale = findViewById(R.id.tvMale);
        tvFemale = findViewById(R.id.tvFemale);
        ivMale = findViewById(R.id.ivMale);
        ivFemale = findViewById(R.id.ivFemale);
        btnSave = findViewById(R.id.btnSave);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        if (getIntent() != null && getIntent().getStringExtra(AppConstant.GENDER) != null) {
            progress.setVisibility(View.GONE);
            String gen = getIntent().getStringExtra(AppConstant.GENDER);
            ivClose.setImageResource(R.mipmap.close);
            if (gen.equals(getString(R.string.male))) {
                maleSelect();
            } else {
                femaleSelect();
            }
        } else {
            // maleSelect();
        }

        ivClose.setOnClickListener(this);
        layoutMale.setOnClickListener(this);
        layoutFemale.setOnClickListener(this);
        ivClose.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        fromCreateAccount();
    }

    private void fromCreateAccount() {
        ProgressBar progress = findViewById(R.id.progress);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            boolean isFromCreateAc = bundle.getBoolean(KEY_IS_FROM_CREATE_AC);
            if (isFromCreateAc) {
                ivClose.setImageResource(R.mipmap.back);
                progress.setVisibility(View.VISIBLE);
                btnSave.setText(getString(R.string.next));
            }
        }
    }

    private void maleSelect() {
        isFemaleSelected = false;
        tvMale.setTextColor(ContextCompat.getColor(this, R.color.orange));
        tvFemale.setTextColor(ContextCompat.getColor(this, R.color.white));
        layoutMale.setBackground(ContextCompat.getDrawable(this, R.drawable.transpirant_orange_boarder_5dp));
        layoutFemale.setBackground(ContextCompat.getDrawable(this, R.drawable.transpirant_white_boarder));
        ivMale.setColorFilter(ContextCompat.getColor(this, R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);
        ivFemale.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);

    }

    private void femaleSelect() {
        isFemaleSelected = true;
        tvMale.setTextColor(ContextCompat.getColor(this, R.color.white));
        tvFemale.setTextColor(ContextCompat.getColor(this, R.color.orange));
        layoutMale.setBackground(ContextCompat.getDrawable(this, R.drawable.transpirant_white_boarder));
        layoutFemale.setBackground(ContextCompat.getDrawable(this, R.drawable.transpirant_orange_boarder_5dp));
        ivMale.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
        ivFemale.setColorFilter(ContextCompat.getColor(this, R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ivClose:
                finish();
                break;

            case R.id.layoutMale:
                maleSelect();
                break;

            case R.id.layoutFemale:
                femaleSelect();
                break;

            case R.id.btnSave:
                if (btnSave.getText().toString().equalsIgnoreCase(getString(R.string.save))) {
                    Intent intent = new Intent();
                    if (isFemaleSelected)
                        intent.putExtra(AppConstant.GENDER, getString(R.string.female));
                    else
                        intent.putExtra(AppConstant.GENDER, getString(R.string.male));

                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    if (isFemaleSelected == null) {
                        // for next screen used in signup flow
                        Snackbar snackbar = ViewUtils.showCustomSnackBar(this, coordinatorLayout, getString(R.string.please_choose_gender));
                        snackbar.show();
                        return;
                    } else {
                        if (isFemaleSelected)
                            signUpData.setGender("2");
                        else
                            signUpData.setGender("1");
                    }
                    Intent intent = new Intent(this, HeightActivity.class);
                    intent.putExtra(KEY_IS_FROM_CREATE_AC, true);
                    intent.putExtra(KEY_SIGN_UP_DATA, signUpData);
                    startActivity(intent);
                }
                break;
        }

    }
}
