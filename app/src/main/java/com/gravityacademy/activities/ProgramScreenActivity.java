package com.gravityacademy.activities;

import android.graphics.Color;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.adapters.HomeFragmentAdapter;
import com.gravityacademy.adapters.ProgramAdapter;
import com.gravityacademy.model.CategoryModel;

import java.util.ArrayList;

public class ProgramScreenActivity extends AppCompatActivity implements View.OnClickListener {

    private CollapsingToolbarLayout toolbar_layout;
    private ImageView ivArrowDown;
    private TextView tvHandstandIsAct;
    private boolean isShowContent;
    Toolbar toolbar;

    private RecyclerView recyclerView;
    private ProgramAdapter mAdapter;
    private ArrayList<CategoryModel> categoryModels;
    private LinearLayoutManager mLinearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_screen);
        setToolBar();
        initViewIds();
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        categoryModels = new ArrayList<>();
        prepairList();
        mAdapter = new ProgramAdapter(this, categoryModels);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void prepairList() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.workouts));
        categoryModel.setSubTitle(getString(R.string.workouts_archiv));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.training_plans));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.training);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.handstand));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.exercises));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.calisthenics_weights));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.strength);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.equipment));
        categoryModel.setTitle(getString(R.string.weeks));
        categoryModel.setSubTitle(getString(R.string.trainingsequipment));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.equipment);
        categoryModels.add(categoryModel);

    }

    private void setToolBar(){
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
      //  toolbar.setBackgroundColor(Color.WHITE);
        toolbar.setNavigationIcon(R.mipmap.back);
       /// toolbar.setBac
        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    finish();
            }
        });
    }

    private void initViewIds(){
        recyclerView = findViewById(R.id.recyclerView);
        toolbar_layout = findViewById(R.id.toolbar_layout);
        ivArrowDown = findViewById(R.id.ivArrowDown);
        tvHandstandIsAct = findViewById(R.id.tvHandstandIsAct);
        ivArrowDown.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivArrowDown:
                if (!isShowContent){
                    isShowContent = true;
                    toolbar_layout.setBackgroundResource(R.mipmap.tool_bar_background);
                    tvHandstandIsAct.setLines(10);
                    ivArrowDown.setImageResource(R.mipmap.arrow_up);
                }else {
                    isShowContent = false;
                    toolbar_layout.setBackgroundResource(R.mipmap.tool_bar_background_small);
                    tvHandstandIsAct.setLines(2);
                    ivArrowDown.setImageResource(R.mipmap.arrow_down);
                }
                break;
        }
    }
}
