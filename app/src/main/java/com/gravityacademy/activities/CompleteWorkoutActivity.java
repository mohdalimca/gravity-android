package com.gravityacademy.activities;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gravityacademy.R;

import java.util.ArrayList;

public class CompleteWorkoutActivity extends AppCompatActivity {

    private TextView tvFire;
    private TextView tvMotivate;
    private TextView tvDestroyed;
    private TextView tvSelfConscious;
    private TextView tvRelaxed;
    private TextView tvTot;
    private TextView tvStark;
    private TextView tvPumped;
    private TextView tvHappy;
    private TextView tvChanged;
    private TextView tvExpended;
    private TextView tvKnockOut;
    private TextView tvEnergetic;
    private TextView tvWeak;
    private TextView tvFinished;
    private Button btnSave;
    private TextView tvFresh;
    private ArrayList<String> selectedImogy = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_workout);
        init();
    }

    private void init() {
        tvFire = findViewById(R.id.tvFire);
        tvFresh = findViewById(R.id.tvFresh);
        tvMotivate = findViewById(R.id.tvMotivate);
        tvDestroyed = findViewById(R.id.tvDestroyed);
        tvSelfConscious = findViewById(R.id.tvSelfConscious);
        tvRelaxed = findViewById(R.id.tvRelaxed);
        tvTot = findViewById(R.id.tvTot);
        tvStark = findViewById(R.id.tvStark);
        tvPumped = findViewById(R.id.tvPumped);
        tvHappy = findViewById(R.id.tvHappy);
        tvChanged = findViewById(R.id.tvChanged);
        tvExpended = findViewById(R.id.tvExpended);
        tvKnockOut = findViewById(R.id.tvKnockOut);
        tvEnergetic = findViewById(R.id.tvEnergetic);
        tvWeak = findViewById(R.id.tvWeak);
        tvFinished = findViewById(R.id.tvFinished);
        btnSave = findViewById(R.id.btnSave);
        performClick(tvFire);
        performClick(tvFresh);
        performClick(tvMotivate);
        performClick(tvDestroyed);
        performClick(tvSelfConscious);
        performClick(tvRelaxed);
        performClick(tvTot);
        performClick(tvStark);
        performClick(tvPumped);
        performClick(tvHappy);
        performClick(tvChanged);
        performClick(tvExpended);
        performClick(tvKnockOut);
        performClick(tvEnergetic);
        performClick(tvWeak);
        performClick(tvFinished);
        performButtonClick(btnSave);
    }

    private void performClick(final TextView textView) {

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageTextViewBackground(tvFire);
                manageTextViewBackground(tvFresh);
                manageTextViewBackground(tvMotivate);
                manageTextViewBackground(tvDestroyed);
                manageTextViewBackground(tvSelfConscious);
                manageTextViewBackground(tvRelaxed);
                manageTextViewBackground(tvTot);
                manageTextViewBackground(tvStark);
                manageTextViewBackground(tvPumped);
                manageTextViewBackground(tvHappy);
                manageTextViewBackground(tvChanged);
                manageTextViewBackground(tvExpended);
                manageTextViewBackground(tvKnockOut);
                manageTextViewBackground(tvEnergetic);
                manageTextViewBackground(tvWeak);
                manageTextViewBackground(tvFinished);

                String text = textView.getText().toString();
                selectedImogy.clear();
                selectedImogy.add(text);
                textView.setTag(String.valueOf(1));
                textView.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.black));
                textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

                /*String tagId = textView.getTag().toString();
                if (tagId.equals("0")) {
                    selectedImogy.add(textView.getText().toString());
                    textView.setTag(String.valueOf(1));
                    textView.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.black));
                    textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                } else {
                    String text = textView.getText().toString();
                    selectedImogy.remove(text);
                    textView.setTag(String.valueOf(0));
                    textView.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.light_white));
                    textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                }*/
            }
        });

    }

    private void manageTextViewBackground(TextView textView){
        textView.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.light_white));
        textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
    }

    private void performButtonClick(final Button btnSave) {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
