package com.gravityacademy.activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerErrorListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerReadyListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerStateListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerTimeListener;
import com.ct7ct7ct7.androidvimeoplayer.model.TextTrack;
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView;
import com.gravityacademy.R;

public class IntroVideoActivity extends AppCompatActivity {
    private VimeoPlayerView vimeoPlayer;
    private int playerState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_video);
        vimeoPlayer = findViewById(R.id.vimeoPlayer);
        vimeoPlayer.setTag(String.valueOf(0));
        setupView();


    }

    private void setupView() {
        // lifecycle.addObserver(vimeoPlayer);
        vimeoPlayer.initialize(true, 362009469);
        vimeoPlayer.defaultColor= Color.RED;

        //vimeoPlayer.initialize(true, {YourPrivateVideoId}, "SettingsEmbeddedUrl")
        //vimeoPlayer.initialize(true, {YourPrivateVideoId},"VideoHashKey", "SettingsEmbeddedUrl")

        vimeoPlayer.addTimeListener(new VimeoPlayerTimeListener() {
            @Override
            public void onCurrentSecond(float second) {
                //  playerCurrentTimeTextView.text = getString(R.string.player_current_time, second.toString());
            }
        });


        vimeoPlayer.addErrorListener(new VimeoPlayerErrorListener() {
            @Override
            public void onError(String message, String method, String name) {
                Toast.makeText(IntroVideoActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });


        vimeoPlayer.addReadyListener(new VimeoPlayerReadyListener() {
            @Override
            public void onReady(String title, float duration, TextTrack[] textTrackArray) {
                playerState = 0;
                // btnPlayPause.setVisibility(View.VISIBLE);
            }

            @Override
            public void onInitFailed() {
                playerState = -1;
            }
        });


        vimeoPlayer.addStateListener(new VimeoPlayerStateListener() {
            @Override
            public void onPlaying(float duration) {
                playerState = 1;

            }

            @Override
            public void onPaused(float seconds) {
                playerState = 2;

            }

            @Override
            public void onEnded(float duration) {
                playerState = 3;

            }
        });
    }

}
