package com.gravityacademy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.response.CommonResponse;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;
import com.gravityacademy.utils.ViewUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnSend;
    private TextView tvSignUp;
    private EditText etEmail;
    private ImageView ivBack;
    private LinearLayout layoutProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initView();
    }

    private void initView() {

        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        btnSend = findViewById(R.id.btnSend);
        tvSignUp = findViewById(R.id.tvSignUp);
        etEmail = findViewById(R.id.etEmail);
        ivBack = findViewById(R.id.ivBack);

        btnSend.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSend:
                if (!AppUtils.isValidateEmail(ForgotPasswordActivity.this, etEmail)) {
                    showSnackBar(getString(R.string.email_is_invalid));
                } else {
                    callForgotPasswordAPI();
                }
                break;

            case R.id.ivBack:
                onBackPressed();
                break;

            case R.id.tvSignUp:
                Intent intent = new Intent(ForgotPasswordActivity.this, GenderSelectActivity.class);
                intent.putExtra(KEY_IS_FROM_CREATE_AC, true);
                startActivity(intent);
                break;
        }

    }

    private void showSnackBar(String errorMessage) {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinatorLayout);
        final Snackbar snackbar = ViewUtils.showCustomSnackBar(ForgotPasswordActivity.this, coordinatorLayout, errorMessage);
        snackbar.show();
    }

    private void callForgotPasswordAPI(){
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().forgotPassword(etEmail.getText().toString()
        ).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!ForgotPasswordActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponse_code() == SUCCESS_RESPONSE_CODE) {
                            Utility.showToastString(ForgotPasswordActivity.this, response.body().getMessage());
                            startActivity(new Intent(ForgotPasswordActivity.this, NewPasswordActivity.class));
                        } else {
                            Utility.showToast(ForgotPasswordActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null){
                        Utility.showToastString(ForgotPasswordActivity.this, AppUtils.getError(response));
                    }else {
                        Utility.showToast(ForgotPasswordActivity.this, R.string.something_went_wrong);
                    }
                }
            }
            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!ForgotPasswordActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(ForgotPasswordActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }
}
