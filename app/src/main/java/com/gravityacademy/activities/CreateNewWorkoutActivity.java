package com.gravityacademy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gravityacademy.R;

public class CreateNewWorkoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_workout);
    }
}
