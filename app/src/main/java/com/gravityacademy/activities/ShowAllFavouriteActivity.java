package com.gravityacademy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gravityacademy.R;
import com.gravityacademy.adapters.ExercisesAdapter;
import com.gravityacademy.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;

public class ShowAllFavouriteActivity extends AppCompatActivity {
    private ExercisesAdapter mAdapterExercise;
    private List<CategoryModel> exersiseList;
    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView rvCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all_favourite);
        rvCategory = findViewById(R.id.rvCategory);
        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        prepairListExersices();
        setupExercisesRecyclerView();
    }

    private void setupExercisesRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(ShowAllFavouriteActivity.this, LinearLayoutManager.VERTICAL, false);
        rvCategory.setLayoutManager(mLinearLayoutManager);
        mAdapterExercise = new ExercisesAdapter(ShowAllFavouriteActivity.this, exersiseList);
        rvCategory.setAdapter(mAdapterExercise);
        rvCategory.setNestedScrollingEnabled(false);
    }

    private void prepairListExersices() {
        exersiseList = new ArrayList<>();
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.chest));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle(getString(R.string.traisap));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.training);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.chest));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle(getString(R.string.traisap));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.training);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);


    }

}
