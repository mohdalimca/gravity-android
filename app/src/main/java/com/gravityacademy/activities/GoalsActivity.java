package com.gravityacademy.activities;

import android.content.Intent;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.utils.AppConstant;
import com.super_rabbit.wheel_picker.WheelPicker;

import java.util.ArrayList;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.KEY_SIGN_UP_DATA;

public class GoalsActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout newbie_layout;
    private LinearLayout beginner_layout;
    private LinearLayout intermediate_layout;
    private LinearLayout advanced_layout;

    private TextView newbie_tv;
    private TextView beginner_tv;
    private TextView intermediate_tv;
    private TextView advanced_tv;

    private TextView i_never_tv;
    private TextView some_exp_tv;
    private TextView moderate_exp_tv;
    private TextView very_exp_tv;

    ImageView ivClose;
    Button btnSave;
    boolean isBuildStrength;
    boolean isBuildMuscle;
    boolean isLoseFat;
    boolean isLearnTech;
    ProgressBar progress;
    private SignUpData signUpData;

    private ArrayList<String> selectedIList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goals);
        initView();
        getBundleData();
        fromCreateAccount();
        getSignUpData();
    }
    private void getSignUpData() {
        if (getIntent() != null && getIntent().getSerializableExtra(KEY_SIGN_UP_DATA) != null) {
            signUpData = (SignUpData) getIntent().getSerializableExtra(KEY_SIGN_UP_DATA);
        }
    }
    private void getBundleData(){
        if (getIntent() != null && getIntent().getStringExtra(AppConstant.AIM) != null) {
            progress.setVisibility(View.GONE);
            String str = getIntent().getStringExtra(AppConstant.AIM);
            if (str.contains(getString(R.string.build_strength))){
                isBuildStrength = true;
                selectedIList.add(getString(R.string.build_strength));
            }
            if (str.contains(getString(R.string.build_muscle))){
                isBuildMuscle = true;
                selectedIList.add(getString(R.string.build_muscle));
            }
            if (str.contains(getString(R.string.lose_fat))){
                isLoseFat = true;
                selectedIList.add(getString(R.string.lose_fat));
            }
            if (str.contains(getString(R.string.learn_tech))){
                isLearnTech = true;
                selectedIList.add(getString(R.string.learn_tech));
            }
            setOrangeLayout();
            ivClose.setImageResource(R.mipmap.close);
        }
    }
    private void initView(){
        selectedIList =  new ArrayList<>();
        progress = findViewById(R.id.progress);
        btnSave = findViewById(R.id.btnSave);
        ivClose = findViewById(R.id.ivClose);


        newbie_layout = findViewById(R.id.newbie_layout);
        beginner_layout = findViewById(R.id.beginner_layout);
        intermediate_layout = findViewById(R.id.intermediate_layout);
        advanced_layout = findViewById(R.id.advanced_layout);

        newbie_tv = findViewById(R.id.newbie_tv);
        beginner_tv = findViewById(R.id.beginner_tv);
        intermediate_tv = findViewById(R.id.intermediate_tv);
        advanced_tv = findViewById(R.id.advanced_tv);

        i_never_tv = findViewById(R.id.i_never_tv);
        some_exp_tv = findViewById(R.id.some_exp_tv);
        moderate_exp_tv = findViewById(R.id.moderate_exp_tv);
        very_exp_tv = findViewById(R.id.very_exp_tv);

        ivClose.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        newbie_layout.setOnClickListener(this);
        beginner_layout.setOnClickListener(this);
        intermediate_layout.setOnClickListener(this);
        advanced_layout.setOnClickListener(this);
    }

    private void fromCreateAccount(){
        ProgressBar progress = findViewById(R.id.progress);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            boolean isFromCreateAc = bundle.getBoolean(KEY_IS_FROM_CREATE_AC);
            if (isFromCreateAc){
                ivClose.setImageResource(R.mipmap.back);
                progress.setVisibility(View.VISIBLE);
                btnSave.setText(getString(R.string.next));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.newbie_layout:
                if (isBuildStrength){
                    selectedIList.remove(newbie_tv.getText().toString());
                    isBuildStrength = false;
                }else {
                    selectedIList.add(newbie_tv.getText().toString());
                    isBuildStrength = true;
                }
                setOrangeLayout();
                break;
            case R.id.beginner_layout:
                if (isBuildMuscle){
                    selectedIList.remove(beginner_tv.getText().toString());
                    isBuildMuscle = false;
                }else {
                    selectedIList.add(beginner_tv.getText().toString());
                    isBuildMuscle = true;
                }
                setOrangeLayout();
                break;
            case R.id.intermediate_layout:
                if (isLoseFat){
                    selectedIList.remove(intermediate_tv.getText().toString());
                    isLoseFat = false;
                }else {
                    selectedIList.add(intermediate_tv.getText().toString());
                    isLoseFat = true;
                }
                setOrangeLayout();
                break;
            case R.id.advanced_layout:
                if (isLearnTech){
                    selectedIList.remove(advanced_tv.getText().toString());
                    isLearnTech = false;
                }else {
                    selectedIList.add(advanced_tv.getText().toString());
                    isLearnTech = true;
                }
                setOrangeLayout();
                break;
            case R.id.btnSave:
                if (btnSave.getText().toString().equalsIgnoreCase(getString(R.string.save))) {
                    Intent intent = new Intent();
                    intent.putExtra(AppConstant.AIM, getSelectedItem());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    String goalsId = null;
                    if (isBuildStrength){
                        goalsId = "1";
                    }
                    if (isBuildMuscle){
                        goalsId = appendComma(goalsId) + "2";
                    }
                    if (isLoseFat){
                        goalsId = appendComma(goalsId) + "3";
                    }
                    if (isLearnTech){
                        goalsId = appendComma(goalsId) + "4";
                    }
                    if (goalsId != null)
                        signUpData.setGoals(goalsId);
                    Intent intent = new Intent(this, PerformanceActivity.class);
                    intent.putExtra(KEY_IS_FROM_CREATE_AC, true);
                    intent.putExtra(KEY_SIGN_UP_DATA, signUpData);
                    startActivity(intent);
                }
                break;
        }
    }

    private String getSelectedItem(){
        String selectedStr = "";
        for (int i = 0; i < selectedIList.size(); i++) {
            if (!TextUtils.isEmpty(selectedStr)){
                selectedStr = selectedStr + ", "+selectedIList.get(i);
            }else {
                selectedStr = selectedIList.get(i);
            }
        }
        return selectedStr;
    }
    private void setOrangeLayout(){
        if (isBuildStrength){
            newbie_layout.setBackgroundResource(R.drawable.rounded_orange_border_background);
            newbie_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
            i_never_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }else {
            newbie_layout.setBackgroundResource(R.drawable.rounded_border_background);
            newbie_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
            i_never_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        }
        if (isBuildMuscle){
            beginner_layout.setBackgroundResource(R.drawable.rounded_orange_border_background);
            beginner_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
            some_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }else {
            beginner_layout.setBackgroundResource(R.drawable.rounded_border_background);
            beginner_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
            some_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        }
        if (isLoseFat){
            intermediate_layout.setBackgroundResource(R.drawable.rounded_orange_border_background);
            intermediate_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
            moderate_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }else {
            intermediate_layout.setBackgroundResource(R.drawable.rounded_border_background);
            intermediate_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
            moderate_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        }
        if (isLearnTech){
            advanced_layout.setBackgroundResource(R.drawable.rounded_orange_border_background);
            advanced_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
            very_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }else {
            advanced_layout.setBackgroundResource(R.drawable.rounded_border_background);
            advanced_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
            very_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        }
    }

    private void setWhiteLayout(){
        newbie_layout.setBackgroundResource(R.drawable.rounded_border_background);
        newbie_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        i_never_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        beginner_layout.setBackgroundResource(R.drawable.rounded_border_background);
        beginner_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        some_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        intermediate_layout.setBackgroundResource(R.drawable.rounded_border_background);
        intermediate_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        moderate_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));
        advanced_layout.setBackgroundResource(R.drawable.rounded_border_background);
        advanced_tv.setTextColor(ContextCompat.getColor(this, R.color.white));
        very_exp_tv.setTextColor(ContextCompat.getColor(this, R.color.white_dull));

    }

    private String appendComma(String goalsId){
        if (goalsId != null){
            goalsId = goalsId + ",";
            return goalsId;
        }else
            return "";
    }

}
