package com.gravityacademy.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gravityacademy.R;
import com.gravityacademy.adapters.MuscleGroupAdapter;
import com.gravityacademy.model.response.Workout;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StrengthSelectActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivBack;
    TextView tvNext;
    private TextView tvTitle;
    TextView tvFitnessLevel;
    RadioButton rbBeginner;
    RadioButton rbIntermediate;
    RadioButton rbAdvance;
    RadioGroup radioGroup;
    private Workout workout;
    private LinearLayoutManager mMuscleGroupLinearLayoutManager;
    private RecyclerView rvMuscleGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strength_select);
        ivBack = findViewById(R.id.ivBack);
        tvNext = findViewById(R.id.tvNext);
        rbBeginner = findViewById(R.id.rbBeginner);
        rbIntermediate = findViewById(R.id.rbIntermediate);
        rbAdvance = findViewById(R.id.rbAdvance);
        tvFitnessLevel = findViewById(R.id.tvFitnessLevel);
        radioGroup = findViewById(R.id.radioGroup);
        tvTitle = findViewById(R.id.tvTitle);
        rvMuscleGroup = findViewById(R.id.rvMuscleGroup);
        ivBack.setOnClickListener(this);
        tvNext.setOnClickListener(this);
        rbAdvance.setOnClickListener(this);
        rbIntermediate.setOnClickListener(this);
        rbBeginner.setOnClickListener(this);
        getWorkoutObject();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                    int fitnessLevelId=Integer.parseInt(rb.getTag().toString());
                    workout.setFitnessLevelId(fitnessLevelId);
                    tvFitnessLevel.setText(AppUtils.getFitnessLevelById(StrengthSelectActivity.this,fitnessLevelId));
                }
            }
        });
        performLikeDislike();
    }
    private void setupView(Workout workout){
        tvTitle.setText(workout.getTitle());
        AppUtils.setupRecyclerViewMuscleGroup(workout.getMuscleGroups(),rvMuscleGroup,StrengthSelectActivity.this,R.layout.item_muscle_group_white);
    }

    private void getWorkoutObject(){
        if(getIntent()!=null && getIntent().getSerializableExtra(AppConstant.WORKOUT_OBJ)!=null) {
            workout = (Workout) getIntent().getSerializableExtra(AppConstant.WORKOUT_OBJ);
            setupView(workout);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvNext:
                Intent intent=new  Intent(this, StrengthTrainingActivity.class);
                intent.putExtra(AppConstant.WORKOUT_OBJ,workout);
                startActivity(intent);
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
        }

    }
    private void performLikeDislike() {
        final ImageView ivFavourite = findViewById(R.id.ivFavourite);
        ivFavourite.setTag("false");
        ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ivFavourite.getTag().equals("true")) {
                    ivFavourite.setImageResource(R.mipmap.heart_blank);
                    ivFavourite.setTag("false");
                } else {
                    ivFavourite.setImageResource(R.mipmap.heart_fill);
                    ivFavourite.setTag("true");
                }
            }
        });

    }

}
