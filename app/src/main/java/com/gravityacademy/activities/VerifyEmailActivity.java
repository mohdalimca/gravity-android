package com.gravityacademy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.response.CommonResponse;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class VerifyEmailActivity extends AppCompatActivity {


    private String email;
    private LinearLayout layoutProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification);
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        TextView tvEmail = findViewById(R.id.tvEmail);
        email = getIntent().getStringExtra(AppConstant.KEY_EMAIL);
        tvEmail.setText(email);
        findViewById(R.id.btnVerifyEmail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callVerifyEmailAPI();
            }
        });
    }

    private void callVerifyEmailAPI() {
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().forgotPassword(email).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!VerifyEmailActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponse_code() == SUCCESS_RESPONSE_CODE) {
                            Utility.showToastString(VerifyEmailActivity.this, response.body().getMessage());
                            startActivity(new Intent(VerifyEmailActivity.this, OTPActivity.class));
                        } else {
                            Utility.showToast(VerifyEmailActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null) {
                        Utility.showToastString(VerifyEmailActivity.this, AppUtils.getError(response));
                    } else {
                        Utility.showToast(VerifyEmailActivity.this, R.string.something_went_wrong);
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!VerifyEmailActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(VerifyEmailActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }

}
