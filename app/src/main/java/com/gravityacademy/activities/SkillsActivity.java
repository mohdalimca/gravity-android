package com.gravityacademy.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.adapters.SkillsAdapter;
import com.gravityacademy.model.CategoryModel;
import com.gravityacademy.utils.AppConstant;

import java.util.ArrayList;

public class SkillsActivity extends AppCompatActivity {

    private RecyclerView rvSkills;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<CategoryModel> exersiseList;
    private SkillsAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skills);
        init();
        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        prepairList();
        setupRecyclerView();
    }

    private void init()
    {   rvSkills=findViewById(R.id.rvSkills);
        TextView tvTitle=findViewById(R.id.tvTitle);
        String title=getIntent().getStringExtra(AppConstant.KEY_TITLE);
        if(title!=null)
        tvTitle.setText(title);
    }

    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvSkills.setLayoutManager(mLinearLayoutManager);
        mAdapter = new SkillsAdapter(this, exersiseList);
        rvSkills.setAdapter(mAdapter);

    }

    private void prepairList() {
        exersiseList=new ArrayList<>();
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.chest));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        exersiseList.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle(getString(R.string.traisap));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.training);
        exersiseList.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel); categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel); categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
    }

}
