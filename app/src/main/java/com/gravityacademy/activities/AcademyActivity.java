package com.gravityacademy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.adapters.AcademyAdapter;
import com.gravityacademy.adapters.ProgramAdapter;
import com.gravityacademy.model.CategoryModel;

import java.util.ArrayList;

public class AcademyActivity extends AppCompatActivity {


    private TextView tvTitle;
    private RecyclerView recyclerView;
    private AcademyAdapter mAdapter;
    private ArrayList<CategoryModel> categoryModels;
    private LinearLayoutManager mLinearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_academy);
        initViewIds();
        setupRecyclerView();
    }
    private void initViewIds(){
        recyclerView = findViewById(R.id.recyclerView);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.academy);
        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        categoryModels = new ArrayList<>();
        prepairList();
        mAdapter = new AcademyAdapter(this, categoryModels);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void prepairList() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.set1));
        categoryModel.setSubTitle(getString(R.string.workouts_archiv));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.progrss_1);
        categoryModel.setImageId2(R.mipmap.progress_guide);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle(getString(R.string.set2));
        categoryModel.setSubTitle(getString(R.string.training_plans));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.technic_1);
        categoryModel.setImageId2(R.mipmap.technik_guide);

        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.set3));
        categoryModel.setSubTitle(getString(R.string.handstand));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.mobility_1);
        categoryModel.setImageId2(R.mipmap.mobility_guide);

        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.exercises));
        categoryModel.setTitle(getString(R.string.set4));
        categoryModel.setSubTitle(getString(R.string.calisthenics_weights));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.strength_1);
        categoryModel.setImageId2(R.mipmap.strenth_guide);

        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.equipment));
        categoryModel.setTitle(getString(R.string.set5));
        categoryModel.setSubTitle(getString(R.string.trainingsequipment));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.streching);
        categoryModel.setImageId2(R.mipmap.streching_guide);

        categoryModels.add(categoryModel);

    }
}
