package com.gravityacademy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.model.response.LoginDataDataModel;
import com.gravityacademy.model.response.LoginDataModel;
import com.gravityacademy.model.response.LoginResponse;
import com.gravityacademy.model.response.MasterDataModel;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.retrofit.DataHandler;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;
import com.gravityacademy.utils.ViewUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.SP_IS_LOG_IN;
import static com.gravityacademy.utils.AppConstant.SP_SESSION_KEY;
import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private EditText etPassword;
    private EditText etEmail;
    private TextView tvForgotPassword;
    private TextView tvVerifyEmail;
    private TextView tvRegister;
    private LinearLayout layoutProgressBar;
    private SignUpData signUpData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        launchHomeScreen();
    }


    private void initView() {
        signUpData = new SignUpData();
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        btnLogin = findViewById(R.id.btnLogin);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        tvVerifyEmail = findViewById(R.id.tvVerifyEmail);
        tvRegister = findViewById(R.id.tvRegister);

        btnLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        tvVerifyEmail.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnLogin:
                if (!AppUtils.isValidateEmail(LoginActivity.this, etEmail)) {
                    showSnackBar(getString(R.string.email_is_invalid));
                } else if (!AppUtils.isValidateString(LoginActivity.this, etPassword)) {
                    showSnackBar(getString(R.string.password_is_invalid));
                } else {
                    signUpData.setEmail(etEmail.getText().toString().trim());
                    signUpData.setPassword(etPassword.getText().toString().trim());
                    callLoginAPI();
                }
                break;

            case R.id.tvForgotPassword:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;

            case R.id.tvVerifyEmail:
                Intent emailIntent = new Intent(LoginActivity.this, VerifyEmailActivity.class);
                if (AppUtils.isValidateEmail(LoginActivity.this, etEmail)) {
                    emailIntent.putExtra(AppConstant.KEY_EMAIL, etEmail.getText().toString().trim());
                    startActivity(emailIntent);
                } else {
                    showSnackBar(getString(R.string.email_is_invalid));
                }
                break;

            case R.id.tvRegister:
                Intent intent = new Intent(LoginActivity.this, GenderSelectActivity.class);
                intent.putExtra(KEY_IS_FROM_CREATE_AC, true);
                startActivity(intent);
                break;
        }

    }


    private void showSnackBar(String errorMessage) {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinatorLayout);
        final Snackbar snackbar = ViewUtils.showCustomSnackBar(LoginActivity.this, coordinatorLayout, errorMessage);
        snackbar.show();
    }

    private void callLoginAPI() {
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().loginUser(signUpData.getDevice_type(),
                signUpData.getEmail(),
                signUpData.getPassword(),
                AppUtils.getAndroidId(LoginActivity.this)
        ).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (!LoginActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponseCode() == SUCCESS_RESPONSE_CODE) {
                            LoginDataDataModel loginData = response.body().getData().getLoginDataDataModel();
                            DataHandler.updatePreferences(SP_SESSION_KEY, loginData.getSessionKey());
                            DataHandler.updatePreferences(SP_IS_LOG_IN, true);
                            Utility.showToastString(LoginActivity.this, response.body().getMessage());
                            launchHomeScreen();
                        } else {
                            Utility.showToast(LoginActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null) {
                        Utility.showToastString(LoginActivity.this, AppUtils.getError(response));
                    } else {
                        Utility.showToast(LoginActivity.this, R.string.something_went_wrong);
                    }
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (!LoginActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(LoginActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }

    private void callMasterDataAPI() {
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().getMasterData().enqueue(new Callback<MasterDataModel>() {
            @Override
            public void onResponse(Call<MasterDataModel> call, Response<MasterDataModel> response) {
                if (!LoginActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponseCode() == SUCCESS_RESPONSE_CODE) {

                        } else {
                            Utility.showToast(LoginActivity.this, R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null) {
                        Utility.showToastString(LoginActivity.this, AppUtils.getError(response));
                    } else {
                        Utility.showToast(LoginActivity.this, R.string.something_went_wrong);
                    }
                }

            }

            @Override
            public void onFailure(Call<MasterDataModel> call, Throwable t) {
                if (!LoginActivity.this.isFinishing()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(LoginActivity.this, R.string.something_went_wrong);
                }
            }
        });
    }

    private void launchHomeScreen() {
        boolean isLogIn = DataHandler.getBooleanPreferences(SP_IS_LOG_IN);
        if (isLogIn) {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finishAffinity();
        }
    }
}
