package com.gravityacademy.activities;

import android.arch.lifecycle.Lifecycle;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerErrorListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerReadyListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerStateListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerTimeListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerVolumeListener;
import com.ct7ct7ct7.androidvimeoplayer.model.TextTrack;
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView;
import com.ct7ct7ct7.androidvimeoplayer.view.menu.ViemoPlayerMenu;
import com.gravityacademy.R;
import com.gravityacademy.adapters.HorizontalAdapter;
import com.gravityacademy.model.CategoryModel;

import java.util.ArrayList;

public class ExerciseDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView rvFitnessLevel, rvMuscleGroups, rvEquipmentNecessary;
    private LinearLayoutManager mLinearLayoutManager1;
    private LinearLayoutManager mLinearLayoutManager2;
    private LinearLayoutManager mLinearLayoutManager3;
    private ArrayList<CategoryModel> exersiseList1, exersiseList2, exersiseList3;
    private HorizontalAdapter mAdapter1, mAdapter2, mAdapter3;
    private VimeoPlayerView vimeoPlayer;
    private Lifecycle lifecycle;
    private int playerState;
    private ImageButton btnPlayPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_detail);
        init();
        setupView();
    }


    private void init() {
        rvFitnessLevel = findViewById(R.id.rvFitnessLevel);
        rvMuscleGroups = findViewById(R.id.rvMuscleGroups);
        vimeoPlayer = findViewById(R.id.vimeoPlayer);
        btnPlayPause = findViewById(R.id.btnPlayPause);
        btnPlayPause.setVisibility(View.INVISIBLE);
        findViewById(R.id.btnStart).setOnClickListener(this);
        findViewById(R.id.ivBack).setOnClickListener(this);
        rvEquipmentNecessary = findViewById(R.id.rvEquipmentNecessary);
        vimeoPlayer.setTag(String.valueOf(0));
        prepairList1();
        prepairList2();
        prepairList3();
        performLikeDislike();
        setupRecyclerViewFitnessLevel();
        setupRecyclerViewMuscleGroups();
        setupRecyclerViewEquipmentNecessary();
    }

    private void setupRecyclerViewFitnessLevel() {
        mLinearLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvFitnessLevel.setLayoutManager(mLinearLayoutManager1);
        mAdapter1 = new HorizontalAdapter(this, exersiseList1);
        rvFitnessLevel.setAdapter(mAdapter1);
        rvFitnessLevel.setNestedScrollingEnabled(false);
        rvFitnessLevel.setHasFixedSize(false);

    }

    private void setupRecyclerViewEquipmentNecessary() {
        mLinearLayoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvEquipmentNecessary.setLayoutManager(mLinearLayoutManager3);
        mAdapter3 = new HorizontalAdapter(this, exersiseList2);
        rvEquipmentNecessary.setAdapter(mAdapter3);
        rvEquipmentNecessary.setNestedScrollingEnabled(false);
        rvEquipmentNecessary.setHasFixedSize(false);

    }

    private void setupRecyclerViewMuscleGroups() {
        mLinearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvMuscleGroups.setLayoutManager(mLinearLayoutManager2);
        mAdapter2 = new HorizontalAdapter(this, exersiseList3);
        rvMuscleGroups.setAdapter(mAdapter2);
        rvMuscleGroups.setNestedScrollingEnabled(false);
        rvMuscleGroups.setHasFixedSize(false);

    }

    private void prepairList1() {
        exersiseList1 = new ArrayList<>();
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.beginner));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        exersiseList1.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle(getString(R.string.medium));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.training);
        exersiseList1.add(categoryModel);

    }

    private void prepairList2() {
        exersiseList2 = new ArrayList<>();
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.chest));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        exersiseList2.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle(getString(R.string.traisap));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.training);
        exersiseList2.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList2.add(categoryModel);
    }

    private void prepairList3() {
        exersiseList3 = new ArrayList<>();
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.no_equipment));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        exersiseList3.add(categoryModel);


    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btnStart:
                Intent intent = new Intent(ExerciseDetailActivity.this, TimerActivity.class);
                startActivity(intent);
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }

    private void performLikeDislike() {
        final ImageView ivFavourite = findViewById(R.id.ivFavourite);
        ivFavourite.setTag("false");
        ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ivFavourite.getTag().equals("true")) {
                    ivFavourite.setImageResource(R.mipmap.heart_blank);
                    ivFavourite.setTag("false");
                } else {
                    ivFavourite.setImageResource(R.mipmap.heart_fill);
                    ivFavourite.setTag("true");
                }
            }
        });

    }

    private void setupView() {
       // lifecycle.addObserver(vimeoPlayer);
        vimeoPlayer.initialize(true, 362009469);
        vimeoPlayer.defaultColor=Color.RED;

        //vimeoPlayer.initialize(true, {YourPrivateVideoId}, "SettingsEmbeddedUrl")
        //vimeoPlayer.initialize(true, {YourPrivateVideoId},"VideoHashKey", "SettingsEmbeddedUrl")

        vimeoPlayer.addTimeListener(new VimeoPlayerTimeListener() {
            @Override
            public void onCurrentSecond(float second) {
                //  playerCurrentTimeTextView.text = getString(R.string.player_current_time, second.toString());
            }
        });


        vimeoPlayer.addErrorListener(new VimeoPlayerErrorListener() {
            @Override
            public void onError(String message, String method, String name) {
                Toast.makeText(ExerciseDetailActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });


        vimeoPlayer.addReadyListener(new VimeoPlayerReadyListener() {
            @Override
            public void onReady(String title, float duration, TextTrack[] textTrackArray) {
                playerState = 0;
               // btnPlayPause.setVisibility(View.VISIBLE);
            }

            @Override
            public void onInitFailed() {
                playerState = -1;
            }
        });


        vimeoPlayer.addStateListener(new VimeoPlayerStateListener() {
            @Override
            public void onPlaying(float duration) {
                playerState = 1;

            }

            @Override
            public void onPaused(float seconds) {
                playerState = 2;

            }

            @Override
            public void onEnded(float duration) {
                playerState = 3;

            }
        });

      /*  volumeSeekBar.progress = 100
        volumeSeekBar.setOnSeekBarChangeListener(object :SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar:SeekBar ?, progress:Int, fromUser:Boolean){
                var volume = progress.toFloat() / 100
                vimeoPlayer.setVolume(volume)
            }

            override fun onStartTrackingTouch(seekBar:SeekBar ?){
            }

            override fun onStopTrackingTouch(seekBar:SeekBar ?){
            }
        })
*/


      /*  vimeoPlayer.addVolumeListener(new VimeoPlayerVolumeListener() {
            @Override
            public void onVolumeChanged(float volume) {

            }
        });
        */

        /*btnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int state = Integer.parseInt(btnPlayPause.getTag().toString());
                if (state == 0) {
                    vimeoPlayer.play();
                    btnPlayPause.setTag(String.valueOf(1));
                }else {
                    vimeoPlayer.pause();
                    btnPlayPause.setTag(String.valueOf(0));


                }
            }
        });*/

       /* getCurrentTimeButton.setOnClickListener {
            Toast.makeText(this, getString(R.string.player_current_time, vimeoPlayer.currentTimeSeconds.toString()), Toast.LENGTH_LONG).show()
        }*/

       /* loadVideoButton.setOnClickListener {
            vimeoPlayer.loadVideo(19231868);
        }
*/
        /*colorButton.setOnClickListener {
            vimeoPlayer.topicColor = Color.GREEN
        }*/
    }

    private void loadVideo(int videoId){
        vimeoPlayer.loadVideo(videoId);

    }

}
