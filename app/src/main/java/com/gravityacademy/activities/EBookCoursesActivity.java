package com.gravityacademy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.adapters.AcademyAdapter;
import com.gravityacademy.adapters.EbookCourseAdapter;
import com.gravityacademy.model.CategoryModel;

import java.util.ArrayList;

public class EBookCoursesActivity extends AppCompatActivity {

    private TextView tvTitle;
    private ImageView ivHeaderLogo;
    private RecyclerView recyclerView;
    private EbookCourseAdapter mAdapter;
    private ArrayList<CategoryModel> categoryModels;
    private LinearLayoutManager mLinearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebook_courses);
        initViewIds();
        setupRecyclerView();
    }

    private void initViewIds(){
        recyclerView = findViewById(R.id.recyclerView);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setVisibility(View.GONE);
        ivHeaderLogo = findViewById(R.id.ivHeaderLogo);
        ivHeaderLogo.setVisibility(View.VISIBLE);

    }


    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        categoryModels = new ArrayList<>();
        prepairList();
        mAdapter = new EbookCourseAdapter(this, categoryModels);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void prepairList() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.workouts));
        categoryModel.setSubTitle(getString(R.string.workouts_archiv));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.drawable.selector_checkbox_1);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.training_plans));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.drawable.selector_checkbox_2);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.training_plans));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.drawable.selector_checkbox_3);
        categoryModels.add(categoryModel);

        /*categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.training_plans));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.unselelcted_course_2);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.handstand));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.selelcted_course_3);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.handstand));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.unselelcted_course_3);
        categoryModels.add(categoryModel);*/

    }
}
