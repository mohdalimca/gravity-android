package com.gravityacademy.activities;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.model.response.SignUpModel;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;
import com.gravityacademy.utils.ViewUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.KEY_SIGN_UP_DATA;
import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout layoutProgressBar;
    private TextView tvLogin;
    private TextView tvLoginBottom;
    private EditText etUserName;
    private EditText etName;
    private EditText etPassword;
    private EditText etEmail;
    private Button btnSignUp;
    private ImageView ivBack;
    private SignUpData signUpData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initView();
        getSignUpData();
    }

    private void initView() {
        etName = findViewById(R.id.etName);
        layoutProgressBar = findViewById(R.id.layoutProgressBar);
        tvLoginBottom = findViewById(R.id.tvLoginBottom);
        btnSignUp = findViewById(R.id.btnSignUp);
        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);
        etEmail = findViewById(R.id.etEmail);
        ivBack = findViewById(R.id.ivBack);

        tvLoginBottom.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        ivBack.setOnClickListener(this);

    }
    private void getSignUpData() {
        if (getIntent() != null && getIntent().getSerializableExtra(KEY_SIGN_UP_DATA) != null) {
            signUpData = (SignUpData) getIntent().getSerializableExtra(KEY_SIGN_UP_DATA);
        }
    }
    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.tvLoginBottom:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSignUp:
                if (!AppUtils.isValidateString(SignUpActivity.this, etUserName)) {
                    showSnackBar(getString(R.string.username_is_invalid));
                } else if (!AppUtils.isValidateEmail(SignUpActivity.this, etEmail)) {
                    showSnackBar(getString(R.string.email_is_invalid));
                } else if (!AppUtils.isValidateString(SignUpActivity.this, etPassword)) {
                    showSnackBar(getString(R.string.password_is_invalid));
                } else {
                    signUpData.setUser_name(etUserName.getText().toString().trim());
                    signUpData.setName(etName.getText().toString().trim());
                    signUpData.setEmail(etEmail.getText().toString().trim());
                    signUpData.setPassword(etPassword.getText().toString().trim());
                    callSignUPAPI();
                   // startActivity(new Intent(SignUpActivity.this, GenderSelectActivity.class));
                }
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
        }

    }

    private void showSnackBar(String errorMessage) {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinatorLayout);
        final Snackbar snackbar = ViewUtils.showCustomSnackBar(SignUpActivity.this, coordinatorLayout, errorMessage);
        snackbar.show();
    }



    private void callSignUPAPI(){
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().signUp(signUpData.getGender(), signUpData.getDevice_type(),
                signUpData.getHeight(), signUpData.getHeight_unit(),
                signUpData.getWeight(), signUpData.getWeight_unit(),
                String.valueOf(signUpData.getFn_level_id()),
                signUpData.getGoals(),
                String.valueOf(signUpData.getMax_pullups()),
                String.valueOf(signUpData.getMax_pushups()),
                String.valueOf(signUpData.getMax_squats()),
                String.valueOf(signUpData.getMax_dips()),
                signUpData.getUser_name(),
                signUpData.getEmail(),
                signUpData.getPassword(),
                signUpData.getName(),
                AppUtils.getAndroidId(SignUpActivity.this)
        ).enqueue(new Callback<SignUpModel>() {
                    @Override
                    public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                        if (!isFinishing()) {
                            layoutProgressBar.setVisibility(View.GONE);
                            if (response != null && response.body() != null) {
                                if (response.body().getResponse_code() == SUCCESS_RESPONSE_CODE) {
                                    SignUpData signUpData1 = response.body().getData();
                                    Utility.showToastString(SignUpActivity.this, response.body().getMessage());
                                } else {
                                    Utility.showToast(SignUpActivity.this, R.string.something_went_wrong);
                                }
                            } else {
                                Utility.showToast(SignUpActivity.this, R.string.something_went_wrong);
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<SignUpModel> call, Throwable t) {
                        if (!isFinishing()) {
                            layoutProgressBar.setVisibility(View.GONE);
                            Utility.showToast(SignUpActivity.this, R.string.something_went_wrong);
                        }
                    }
                });
    }

}
