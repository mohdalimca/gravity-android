package com.gravityacademy.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.gravityacademy.R;
import com.gravityacademy.fragments.BottomFragment;
import com.gravityacademy.fragments.FavouriteFragment;
import com.gravityacademy.fragments.HomeFragment;
import com.gravityacademy.fragments.MemberShipFragment;
import com.gravityacademy.fragments.ProfileFragment;
import com.gravityacademy.fragments.ProgramsFragment;
import com.gravityacademy.fragments.TimelineFragment;
import com.gravityacademy.listeners.BottomOptionClickListener;
import com.gravityacademy.model.response.WorkoutLibraryDataModel;
import com.gravityacademy.model.response.WorkoutLibraryModel;
import com.gravityacademy.model.response.WorkoutOfTheDayDataModel;
import com.gravityacademy.model.response.WorkoutOfTheDayModel;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

public class HomeActivity extends AppCompatActivity implements HomeFragment.OnHomeFragmentInteractionListener,
        ProfileFragment.OnProfileFragmentInteractionListener,
        ProgramsFragment.OnProgramsFragmentInteractionListener,
        MemberShipFragment.OnMemberShipFragmentInteractionListener, BottomOptionClickListener {
    BottomNavigationView navView;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    public FrameLayout bottomFrameLayout;
    private LinearLayout layoutProgressBar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_workout:
                    Fragment fragment = new HomeFragment();
                    setDefaultFragment(fragment);
                    // mTextMessage.setText(R.string.text_not_now);
                    return true;
                case R.id.navigation_favourite:
                    fragment = new ProgramsFragment();
                    setDefaultFragment(fragment);
                    // mTextMessage.setText(R.string.text_settings);
                    return true;
                case R.id.navigation_profile:
                    fragment = new ProfileFragment();
                    setDefaultFragment(fragment);
                    removeBadge(2);
                    // mTextMessage.setText(R.string.app_name);
                    return true;
                case R.id.navigation_acamey:
                    fragment = new MemberShipFragment();
                    setDefaultFragment(fragment);
                    // mTextMessage.setText(R.string.app_name);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bottomFrameLayout = findViewById(R.id.bottomFrameLayout);
        layoutProgressBar = findViewById(R.id.layoutProgressBar);

        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        showBadge(2);
        Fragment fragment = new HomeFragment();
        setDefaultFragment(fragment);
        loadBottomFragment(new BottomFragment());
    }

    private void showBadge(int position) {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) navView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(position);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;
        View badge = LayoutInflater.from(this)
                .inflate(R.layout.notification_badge, itemView, true);
    }

    private void loadBottomFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.bottomFrameLayout, fragment);
        fragmentTransaction.commit();
    }


    private void removeBadge(int position) {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) navView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(position);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;
        View badge = itemView.findViewById(R.id.notifications_badge);
        if (badge != null)
            ((ViewGroup) badge.getParent()).removeView(badge);
    }

    // This method is used to set the default fragment that will be shown.
    private void setDefaultFragment(Fragment defaultFragment) {
        this.replaceFragment(defaultFragment);
    }

    public void replaceFragment(Fragment destFragment) {
        fragmentManager = this.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        Log.e("destFragment: ", destFragment.getClass().getSimpleName());
        fragmentTransaction.replace(R.id.contentLayout, destFragment, destFragment.getClass().getSimpleName());
        /*if (!(destFragment instanceof SplashFragment))
            fragmentTransaction.addToBackStack(destFragment.getClass().getSimpleName());*/
        fragmentTransaction.commit();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onWorkoutButtonClicked() {
        setDefaultFragment(new HomeFragment());
    }

    @Override
    public void onFavouriteButtonClicked() {
        setDefaultFragment(new FavouriteFragment());

    }

    @Override
    public void onTimelineButtonClicked() {
        setDefaultFragment(new TimelineFragment());
    }

    @Override
    public void onAcademyButtonClicked() {
        setDefaultFragment(new ProgramsFragment());
    }

    @Override
    public void onProfileButtonClicked() {
        setDefaultFragment(new ProfileFragment());
    }




}
