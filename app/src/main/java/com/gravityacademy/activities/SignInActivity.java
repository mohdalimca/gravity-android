package com.gravityacademy.activities;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

import com.gravityacademy.R;
import com.gravityacademy.fragments.LoginFragment;
import com.gravityacademy.fragments.RecoverPasswordFragment;
import com.gravityacademy.fragments.SplashFragment;

public class SignInActivity extends AppCompatActivity implements
        LoginFragment.OnFragmentInteractionListener,
        RecoverPasswordFragment.OnFragmentInteractionListener {

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        setToolBar();
        defaultFragment();
    }
    private void setToolBar(){
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.login));
        toolbar.setBackgroundColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (fragmentManager.getBackStackEntryCount() > 0 )
                   fragmentManager.popBackStack();
               else
                   finish();
            }
        });
    }

    private void defaultFragment() {
        // Create and set Android Fragment as default.
        Fragment fragment = new LoginFragment();
        setDefaultFragment(fragment);
    }

    // This method is used to set the default fragment that will be shown.
    private void setDefaultFragment(Fragment defaultFragment) {
        this.replaceFragment(defaultFragment);
    }

    public void replaceFragment(Fragment destFragment) {
        fragmentManager = this.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        Log.e("destFragment: ", destFragment.getClass().getSimpleName());
        fragmentTransaction.replace(R.id.dynamic_fragment_frame_layout, destFragment, destFragment.getClass().getSimpleName());
        if (!(destFragment instanceof LoginFragment))
            fragmentTransaction.addToBackStack(destFragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(Fragment fragment) {
        replaceFragment(fragment);
    }

    @Override
    public void onUIUpdate(Fragment fragment) {

        if (fragment instanceof RecoverPasswordFragment){
            toolbar.setTitle(getString(R.string.recover_password));
        }else {
            toolbar.setTitle(getString(R.string.login));
        }
    }


}
