package com.gravityacademy.activities;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;

import com.gravityacademy.R;
import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.utils.AppConstant;
import com.super_rabbit.wheel_picker.WheelPicker;

import static com.gravityacademy.utils.AppConstant.KEY_IS_FROM_CREATE_AC;
import static com.gravityacademy.utils.AppConstant.KEY_SIGN_UP_DATA;

public class WeightActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivClose;
    Button btnSave;
    RadioButton kilogram_rb;
    RadioButton lbs_rb;
    WheelPicker weight_numberPicker;
    ProgressBar progress;
    private SignUpData signUpData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);
        initView();
        getBundleData();
        fromCreateAccount();
        getSignUpData();
    }
    private void initView(){
        progress = findViewById(R.id.progress);
        btnSave = findViewById(R.id.btnSave);
        ivClose = findViewById(R.id.ivClose);
        weight_numberPicker = findViewById(R.id.weight_numberPicker);
        lbs_rb = findViewById(R.id.lbs_rb);
        kilogram_rb = findViewById(R.id.kilogram_rb);
        kilogram_rb.setChecked(true);

        ivClose.setOnClickListener(this);
        kilogram_rb.setOnClickListener(this);
        lbs_rb.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    private void getSignUpData() {
        if (getIntent() != null && getIntent().getSerializableExtra(KEY_SIGN_UP_DATA) != null) {
            signUpData = (SignUpData) getIntent().getSerializableExtra(KEY_SIGN_UP_DATA);
            signUpData.setWeight_unit("1");
        }
    }

    private void getBundleData(){
        if (getIntent() != null && getIntent().getStringExtra(AppConstant.WEIGHT) != null) {
            progress.setVisibility(View.GONE);
            String str = getIntent().getStringExtra(AppConstant.WEIGHT);
            ivClose.setImageResource(R.mipmap.close);
            String [] splitStr = str.split(" ");
            String unit = splitStr[1];
            if (unit.equalsIgnoreCase(getString(R.string.kilogram))){
                kilogram_rb.setChecked(true);
            }else {
                lbs_rb.setChecked(true);
            }
            resetData();
            weight_numberPicker.scrollToValue(splitStr[0]);
        }
    }

    private void resetData(){
        if (kilogram_rb.isChecked()){
            weight_numberPicker.setMin(40);
            weight_numberPicker.setMax(150);
            weight_numberPicker.invalidate();
        }else {
            weight_numberPicker.setMin(88);
            weight_numberPicker.setMax(330);
            weight_numberPicker.invalidate();
        }
    }

    private void fromCreateAccount(){
        ProgressBar progress = findViewById(R.id.progress);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            boolean isFromCreateAc = bundle.getBoolean(KEY_IS_FROM_CREATE_AC);
            if (isFromCreateAc){
                ivClose.setImageResource(R.mipmap.back);
                progress.setVisibility(View.VISIBLE);
                btnSave.setText(getString(R.string.next));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.kilogram_rb:
                weight_numberPicker.setMin(40);
                weight_numberPicker.setMax(150);
                weight_numberPicker.invalidate();
               // weight_numberPicker.reset();
                break;
            case R.id.lbs_rb:
                weight_numberPicker.setMin(88);
                weight_numberPicker.setMax(330);
                weight_numberPicker.invalidate();
             //   weight_numberPicker.reset();
                break;
            case R.id.ivClose:
                finish();
                break;
            case R.id.btnSave:
                if (btnSave.getText().toString().equalsIgnoreCase(getString(R.string.save))) {
                    Intent intent = new Intent();
                    String unit = "";
                    if (kilogram_rb.isChecked())
                        unit = kilogram_rb.getText().toString();
                    else
                        unit = lbs_rb.getText().toString();
                    intent.putExtra(AppConstant.WEIGHT, weight_numberPicker.getCurrentItem() + " "+unit);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    signUpData.setWeight(weight_numberPicker.getCurrentItem());
                    if (kilogram_rb.isChecked())
                        signUpData.setWeight_unit("1");
                    else
                        signUpData.setWeight_unit("2");
                    Intent intent = new Intent(this, FitnessLevelActivity.class);
                    intent.putExtra(KEY_IS_FROM_CREATE_AC, true);
                    intent.putExtra(KEY_SIGN_UP_DATA, signUpData);
                    startActivity(intent);
                }
                break;
        }
    }
}
