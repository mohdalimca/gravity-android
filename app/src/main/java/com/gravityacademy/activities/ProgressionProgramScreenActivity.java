package com.gravityacademy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gravityacademy.R;
import com.gravityacademy.adapters.ProgramAdapter;
import com.gravityacademy.adapters.ProgressionProgramAdapter;
import com.gravityacademy.model.CategoryModel;

import java.util.ArrayList;

public class ProgressionProgramScreenActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressionProgramAdapter mAdapter;
    private ArrayList<CategoryModel> categoryModels;
    private LinearLayoutManager mLinearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progression_program_screen);

        initViewIds();
        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        categoryModels = new ArrayList<>();
        prepairList();
        mAdapter = new ProgressionProgramAdapter(this, categoryModels);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void prepairList() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.workouts));
        categoryModel.setSubTitle(getString(R.string.workouts_archiv));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.training_plans));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.training);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.handstand));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.exercises));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.calisthenics_weights));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.strength);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.equipment));
        categoryModel.setTitle(getString(R.string.weeks));
        categoryModel.setSubTitle(getString(R.string.trainingsequipment));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.equipment);
        categoryModels.add(categoryModel);

    }

    private void initViewIds(){
        recyclerView = findViewById(R.id.recyclerView);
    }
}
