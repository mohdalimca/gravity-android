package com.gravityacademy.utils;

import com.gravityacademy.BuildConfig;

/**
 * Created by 05/09/19
 */
public class AppConstant {
    public static final String FILE_NAME_SHARED_PREF = "gravity_academy.xml";
    public static final String SP_TOKEN = "token";
    public static final String APP_DIRECTORY ="/" + BuildConfig.APP_NAME ;
    public static final String IMAGE_PREFIX = "IMG_";

    public static final int  SUCCESS_RESPONSE_CODE = 200;
    public static final int  FAILURE_RESPONSE_CODE = 500;

    public static final String SP_SESSION_KEY = "sessionkey";
    public static final String SP_IS_LOG_IN = "isLogIn";


    public static final int  CODE_CAMERA = 201;
    public static final int CODE_CAMERA_CROP = 202;
    public static final int CODE_GALLERY = 203;
    public static final int CODE_GALLERY_CROP = 204;
    public static final int CODE_GENDER_SELECT = 205;
    public static final int CODE_CHANGE_PASSWORD = 206;
    public static final String GENDER = "gender";
    public static final String HEIGHT = "height";
    public static final String WEIGHT = "weight";
    public static final String FITNESS_LEVEL = "fitnessLevel";
    public static final String AIM = "aim";
    public static final String MAX_PULL_UPS = "max_pull_ups";
    public static final String MAX_PUSH_UPS = "max_push_ups";
    public static final String MAX_SQUATS = "max_squats";
    public static final String MAX_DIPS = "max_dips";
    public static final String KEY_IS_FROM_CREATE_AC = "is_create_account";
    public static final String KEY_SIGN_UP_DATA = "signUpData";
    public static final String KEY_TYPE = "type";
    public static final String KEY_TITLE = "title";
    public static final String KEY_EMAIL = "email";
    public static final String Equipment_URL="https://www.journaldev.com";
    public static final String WORKOUT_OBJ="workoutObj";
}
