package com.gravityacademy.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gravityacademy.BuildConfig;
import com.gravityacademy.R;
import com.gravityacademy.activities.HomeActivity;
import com.gravityacademy.activities.LoginActivity;
import com.gravityacademy.activities.StrengthSelectActivity;
import com.gravityacademy.adapters.MuscleGroupAdapter;
import com.gravityacademy.app.AppController;
import com.gravityacademy.model.response.ErrorModel;
import com.gravityacademy.model.response.SignUpModel;
import com.gravityacademy.retrofit.DataHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.SP_SESSION_KEY;

/**
 * Created by 17/09/19
 */
public class AppUtils {

    public static boolean isValidEmail(String error, EditText emailET) {
        String EMAIL_PATTERN = "^.+@.+\\..+$"; //"^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
        String email = emailET.getText().toString();
        Matcher m = emailPattern.matcher(email);
        if (!m.matches()) {
            //   emailET.setError(error);
            emailET.requestFocus();
        }
        return m.matches();
    }

    public static boolean isValidateEmail(Context context, EditText editText) {
        boolean isValid = false;
        String email = editText.getText().toString().trim();
        if (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            isValid = true;
        } else {
            editText.requestFocus();
            isValid = false;
        }
        return isValid;
    }

    public static boolean isValidateString(Context context, EditText editText) {
        boolean isValid = true;
        String email = editText.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            editText.requestFocus();
            isValid = false;
        }
        return isValid;
    }

    public static File getOutputMediaFile(Context context) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File file = new File(context.getExternalFilesDir("images"), "IMG_" + timeStamp + ".jpg");
        return file;
    }

    public static void galleryCapture(Activity activity, int code) {
        try {
            activity.startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    .setType("image/*"), code);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void cameraCapture(Activity activity, File cameraInput, int code, boolean isFrontCamera) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            activity.startActivityForResult(new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                    .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraInput)).putExtra("android.intent.extras.CAMERA_FACING", 1), code);
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri photoURI = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", cameraInput);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            if (isFrontCamera)
                cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            activity.startActivityForResult(cameraIntent, code);
        }
    }

    public static void cameraCrop(Activity activity, File cropInput, File cropOutput, int code) {

        try {
            Intent cropIntent = null;
            if (cropInput != null) {
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                    cropIntent = new Intent("com.android.camera.action.CROP", Uri.fromFile(cropInput));
                    cropIntent.setDataAndType(Uri.fromFile(cropInput), "image/*");
                } else {
                    Uri photoURI = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", cropInput);
                    cropIntent = new Intent("com.android.camera.action.CROP", photoURI);
                    cropIntent.putExtra("aspectX", 1);
                    cropIntent.putExtra("aspectY", 1);
                    //indicate output X and Y
                    //cropIntent.putExtra("outputX", 440);
                    //cropIntent.putExtra("outputY", 640);
                    cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    cropIntent.setDataAndType(photoURI, "image/*");
                }

                cropIntent.putExtra("crop", "true");
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropOutput));

                activity.startActivityForResult(cropIntent, code);
            } else {
                //Utility.showToast(StartInterviewActivity.this, "Unable to take picture, please try again");
            }
        } catch (ActivityNotFoundException anfe) {
            AppUtils.showToast(activity, R.string.txt_no_activity_found_to_handle_intent);
            //Utility.showToastString(activity, anfe.getMessage());
            anfe.printStackTrace();

        }


    }

    public static String isContactValid(String phoneNumber) {

        if (phoneNumber.contains("(")) {
            phoneNumber = phoneNumber.replace("(", "");
        }
        if (phoneNumber.contains(")")) {
            phoneNumber = phoneNumber.replace(")", "");
        }
        if (phoneNumber.contains("-")) {
            phoneNumber = phoneNumber.replace("-", "");
        }
        if (phoneNumber.contains(" ")) {
            phoneNumber = phoneNumber.replace(" ", "");
        }

        return phoneNumber;
    }

    public static void showToast(Context context, int message) {
        Toast toast = Toast.makeText(context, context.getResources().getString(message), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void showToastString(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static boolean isConnectedToInternet(Context context, boolean showToast) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            } else {
                if (showToast) {
                    showToast(context, R.string.check_internet_connection);
                }

                return false;
            }
        } catch (Exception e) {
            if (showToast) {
                showToast(context, R.string.something_went_wrong);
            }

            return false;
        }
    }


    public static void galleryCrop(Activity activity, Uri cropInput, File cropOutput, int code) {

        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP", cropInput);
            cropIntent.setDataAndType(cropInput, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropOutput));
            activity.startActivityForResult(cropIntent, code);
        } catch (ActivityNotFoundException e) {
            AppUtils.showToast(activity, R.string.txt_no_activity_found_to_handle_intent);
            e.printStackTrace();
        } catch (Exception e) {
            AppUtils.showToast(activity, R.string.txt_error_on_crop);
            e.printStackTrace();
        }
    }

    public static void setProfileImage(Context context, String filePath, ImageView ivProfile) {
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
        Glide.with(context)
                .load(stream.toByteArray())
/*
                .error(R.drawable.link_placholder)
*/
                .into(ivProfile);

    }

    public static boolean deleteDir(File dir, Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]), context);
                    if (!success) {
                        return false;
                    }
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static String getAndroidId(Context context){
        String androidId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return androidId;
    }

    public static String getError(Response response){
        Gson gson = new Gson();
        Type type = new TypeToken<ErrorModel>() {}.getType();
        ErrorModel errorResponse = gson.fromJson(response.errorBody().charStream(),type);
        return errorResponse.getError();
    }

    public static void onLogout(Activity activity){
        DataHandler.updatePreferences(AppConstant.SP_IS_LOG_IN, false);
        DataHandler.updatePreferences(SP_SESSION_KEY, "");
        activity.finishAffinity();
        activity.startActivity(new Intent(activity, LoginActivity.class));
    }

    public static String loadJSONFromAsset(Activity activity,String fileName) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    @SuppressLint("SimpleDateFormat")
    public static String changeDateFormat(String strCurrentDate){
        //String strCurrentDate = "2019-12-08 23:43:17";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("dd MMM yyyy");
        String date = format.format(newDate);
        return date;
    }
    public static String getFitnessLevelById(Context context,int fitnessLevelId){
        String fitnessLevel="";
        switch (fitnessLevelId) {
            case 1:
                fitnessLevel= context.getString(R.string.beginner);
                break;
            case 2:
                fitnessLevel= context.getString(R.string.intermediate);
                break;
            case 3:
                fitnessLevel= context.getString(R.string.advanced);
                break;
        }
        return fitnessLevel;
    }

    public static void setupRecyclerViewMuscleGroup(String muscleGroup, RecyclerView rvMuscleGroup,Context context,int resourceID) {
        String [] strings = muscleGroup.split(",");
        List<String> muscleGroups = new ArrayList<>(Arrays.asList(strings)); //new ArrayList is only needed if you absolutely need an ArrayList
        LinearLayoutManager mMuscleGroupLinearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rvMuscleGroup.setLayoutManager(mMuscleGroupLinearLayoutManager);
        MuscleGroupAdapter mAdapter = new MuscleGroupAdapter(context, muscleGroups,resourceID);
        rvMuscleGroup.setAdapter(mAdapter);
    }

}
