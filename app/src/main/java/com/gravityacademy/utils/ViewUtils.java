package com.gravityacademy.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.gravityacademy.R;

/**
 * Created by 17/09/19
 */
public class ViewUtils {

    public static void snackBarView(View v, String msg){
        CoordinatorLayout coordinatorLayout= v.findViewById(R.id.coordinatorLayout);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        CoordinatorLayout.LayoutParams params=(CoordinatorLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setRotation(180);
        view.setBackgroundColor(Color.RED);
        TextView mainTextView = view.findViewById(android.support.design.R.id.snackbar_text);
        mainTextView.setTextColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        mainTextView.setTypeface(null, Typeface.BOLD);
       // mainTextView.setTextSize(12);
        view.setLayoutParams(params);
        snackbar.show();
    }

    public static void customSnackBar(Context context, View v, String msg){
        Snackbar snackbar = Snackbar.make(v, "", Snackbar.LENGTH_LONG);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        LayoutInflater mInflater = LayoutInflater.from(context);
        View snackView = mInflater.inflate(R.layout.app_bar_main, null);
        TextView textViewTop =  snackView.findViewById(R.id.title_tv);
        textViewTop.setText(msg);
        textViewTop.setTextColor(Color.WHITE);
        layout.setPadding(0,0,0,0);
        layout.addView(snackView, 0);
        snackbar.show();
    }


    public static Snackbar showCustomSnackBar(Context context, CoordinatorLayout coordinatorLayout, String msg) { // Create the Snackbar
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "", Snackbar.LENGTH_LONG);
        // 15 is margin from all the sides for snackbar
        int marginFromSides = 0;
        float height = 80;
        LayoutInflater mInflater = LayoutInflater.from(context);
        //inflate view
        View snackView = mInflater.inflate(R.layout.text_view, null);
        TextView textView = snackView.findViewById(R.id.text_view);
        textView.setText(msg);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.red));
     //   snackbar.getView().setBackground(context.getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        Snackbar.SnackbarLayout snackBarView = (Snackbar.SnackbarLayout) snackbar.getView();
        CoordinatorLayout.LayoutParams parentParams = (CoordinatorLayout.LayoutParams) snackBarView.getLayoutParams();
        parentParams.setMargins(marginFromSides, 0, marginFromSides, marginFromSides);
        parentParams.height = (int) height;
        parentParams.width = FrameLayout.LayoutParams.MATCH_PARENT;
        snackBarView.setLayoutParams(parentParams);

        snackBarView.addView(snackView, 0);
        return snackbar;
    }

}
