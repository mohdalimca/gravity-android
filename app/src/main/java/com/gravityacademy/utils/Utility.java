package com.gravityacademy.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.Toast;

import com.gravityacademy.BuildConfig;
import com.gravityacademy.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.LOCATION_SERVICE;

public class Utility {

    public static boolean isStringValid(String str) {

        if (TextUtils.isEmpty(str) || str.trim().length() == 0) {
            return false;
        }

        return true;
    }

    public static String isContactValid(String phoneNumber) {

        if (phoneNumber.contains("(")) {
            phoneNumber = phoneNumber.replace("(", "");
        }
        if (phoneNumber.contains(")")) {
            phoneNumber = phoneNumber.replace(")", "");
        }
        if (phoneNumber.contains("-")) {
            phoneNumber = phoneNumber.replace("-", "");
        }
        if (phoneNumber.contains(" ")) {
            phoneNumber = phoneNumber.replace(" ", "");
        }

        return phoneNumber;
    }

    public static void showToast(Context context, int message) {
        Toast toast = Toast.makeText(context, context.getResources().getString(message), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void showToastString(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static boolean isConnectedToInternet(Context context, boolean showToast) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            } else {
                if (showToast) {
                    showToast(context, R.string.check_internet_connection);
                }

                return false;
            }
        } catch (Exception e) {
            if (showToast) {
                showToast(context, R.string.something_went_wrong);
            }

            return false;
        }
    }


    public static void startOpenWebPage(Context context, String url) {

        if (!url.trim().startsWith("http://") && !url.trim().startsWith("https://")) {
            url = "http://" + url;
        }
        Uri webpage = Uri.parse(url);

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Utility.showToast(context, R.string.txt_no_activity_found_to_handle_intent);
            e.printStackTrace();
        } catch (Exception e) {
            if (url.startsWith("http://")) {
                startOpenWebPage(context, url.replace("http://", "https://"));
            }
        }
    }


    /**
     * To check String for empty or null if String is null or empty return true
     *
     * @param value
     * @return
     */

    public static boolean isEmpty(String value) {
        String empty = "";
        return value == null || empty.equals(value.trim());
    }


    public static void galleryCapture(Activity activity, int code) {
        try {
            activity.startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    .setType("image/*"), code);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void cameraCapture(Activity activity, File cameraInput, int code, boolean isFrontCamera) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            activity.startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cameraInput)).putExtra("android.intent.extras.CAMERA_FACING", 1), code);
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri photoURI = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", cameraInput);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            if (isFrontCamera)
                cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            else
                cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 0);
            activity.startActivityForResult(cameraIntent, code);
        }
    }

    public static void cameraCrop(Activity activity, File cropInput, File cropOutput, int code) {

        try {
            Intent cropIntent = null;
            if (cropInput != null) {
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                    cropIntent = new Intent("com.android.camera.action.CROP", Uri.fromFile(cropInput));
                    cropIntent.setDataAndType(Uri.fromFile(cropInput), "image/*");
                } else {
                    Uri photoURI = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", cropInput);
                    cropIntent = new Intent("com.android.camera.action.CROP", photoURI);
                    cropIntent.putExtra("aspectX", 1);
                    cropIntent.putExtra("aspectY", 1);
                    //indicate output X and Y
                    //cropIntent.putExtra("outputX", 440);
                    //cropIntent.putExtra("outputY", 640);
                    cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    cropIntent.setDataAndType(photoURI, "image/*");
                }

                cropIntent.putExtra("crop", "true");
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropOutput));

                activity.startActivityForResult(cropIntent, code);
            } else {
                //Utility.showToast(StartInterviewActivity.this, "Unable to take picture, please try again");
            }
        } catch (ActivityNotFoundException anfe) {
            Utility.showToast(activity, R.string.txt_no_activity_found_to_handle_intent);
            //Utility.showToastString(activity, anfe.getMessage());
            anfe.printStackTrace();

        }


    }

    public static void galleryCrop(Activity activity, Uri cropInput, File cropOutput, int code) {

        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP", cropInput);
            cropIntent.setDataAndType(cropInput, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropOutput));
            activity.startActivityForResult(cropIntent, code);
        } catch (ActivityNotFoundException e) {
            Utility.showToast(activity, R.string.txt_no_activity_found_to_handle_intent);
            e.printStackTrace();
        } catch (Exception e) {
            Utility.showToast(activity, R.string.txt_error_on_crop);
            e.printStackTrace();
        }
    }


    public static File getOutputMediaFile(Context context) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File file = new File(context.getExternalFilesDir("images"), "IMG_" + timeStamp + ".jpg");
        return file;
    }

    public static boolean isGpsEnabled(Context context) {
        LocationManager mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        }
        return false;
    }


}
