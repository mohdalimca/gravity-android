
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExerciseLibraryModel extends CommonDataResponseModel implements Serializable {

    @SerializedName("data")
    @Expose
    private ExerciseLibraryDataModel data;

    public ExerciseLibraryDataModel getData() {
        return data;
    }

    public void setData(ExerciseLibraryDataModel data) {
        this.data = data;
    }

}
