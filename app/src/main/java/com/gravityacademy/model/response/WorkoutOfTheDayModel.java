
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WorkoutOfTheDayModel extends CommonDataResponseModel implements Serializable {


    @SerializedName("data")
    @Expose
    private WorkoutOfTheDayDataModel data;

    public WorkoutOfTheDayDataModel getData() {
        return data;
    }

    public void setData(WorkoutOfTheDayDataModel data) {
        this.data = data;
    }
}
