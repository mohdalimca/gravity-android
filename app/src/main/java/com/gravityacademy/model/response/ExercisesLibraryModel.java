
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExercisesLibraryModel extends CommonDataResponseModel implements Serializable {


    @SerializedName("data")
    @Expose
    private ExercisesLibraryDataModel data;

    public ExercisesLibraryDataModel getData() {
        return data;
    }

    public void setData(ExercisesLibraryDataModel data) {
        this.data = data;
    }
}
