
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FitnessLevel {

    @SerializedName("fn_level_id")
    @Expose
    private String fnLevelId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;

    public String getFnLevelId() {
        return fnLevelId;
    }

    public void setFnLevelId(String fnLevelId) {
        this.fnLevelId = fnLevelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
