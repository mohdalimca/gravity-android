package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginDataModel implements Serializable {


    @SerializedName("login_data")
    @Expose
    private LoginDataDataModel loginDataDataModel;

    public LoginDataDataModel getLoginDataDataModel() {
        return loginDataDataModel;
    }

    public void setLoginDataDataModel(LoginDataDataModel loginDataDataModel) {
        this.loginDataDataModel = loginDataDataModel;
    }


}
