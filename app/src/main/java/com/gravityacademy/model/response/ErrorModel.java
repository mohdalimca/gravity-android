package com.gravityacademy.model.response;

public class ErrorModel {
    private String error;
    private String global_error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getGlobal_error() {
        return global_error;
    }

    public void setGlobal_error(String global_error) {
        this.global_error = global_error;
    }

}
