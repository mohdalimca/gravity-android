
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExerciseLibraryDataModel implements Serializable {

    @SerializedName("exercise_lib")
    @Expose
    private ExerciseLib exerciseLib;

    public ExerciseLib getExerciseLib() {
        return exerciseLib;
    }

    public void setExerciseLib(ExerciseLib exerciseLib) {
        this.exerciseLib = exerciseLib;
    }

}
