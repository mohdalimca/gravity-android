
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WorkoutLibraryModel extends CommonDataResponseModel implements Serializable {


    @SerializedName("data")
    @Expose
    private WorkoutLibraryDataModel data;

    public WorkoutLibraryDataModel getData() {
        return data;
    }

    public void setData(WorkoutLibraryDataModel data) {
        this.data = data;
    }

}
