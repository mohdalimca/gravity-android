package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse extends CommonDataResponseModel implements Serializable {

    @SerializedName("data")
    @Expose
    private LoginDataModel data;

    public LoginDataModel getData() {
        return data;
    }

    public void setData(LoginDataModel data) {
        this.data = data;
    }
}
