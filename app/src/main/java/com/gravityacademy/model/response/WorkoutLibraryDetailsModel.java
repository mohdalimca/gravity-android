
package com.gravityacademy.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkoutLibraryDetailsModel {

    @SerializedName("workout_category_id")
    @Expose
    private String workoutCategoryId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("workout_count")
    @Expose
    private String workoutCount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("workouts")
    @Expose
    private List<Workout> workouts = null;

    public String getWorkoutCategoryId() {
        return workoutCategoryId;
    }

    public void setWorkoutCategoryId(String workoutCategoryId) {
        this.workoutCategoryId = workoutCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWorkoutCount() {
        return workoutCount;
    }

    public void setWorkoutCount(String workoutCount) {
        this.workoutCount = workoutCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(List<Workout> workouts) {
        this.workouts = workouts;
    }

}
