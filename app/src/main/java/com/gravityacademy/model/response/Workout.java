
package com.gravityacademy.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Workout implements Serializable {

    @SerializedName("workout_id")
    @Expose
    private String workoutId;
    @SerializedName("workout_category_id")
    @Expose
    private String workoutCategoryId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("workout_goal")
    @Expose
    private String workoutGoal;
    @SerializedName("muscle_groups")
    @Expose
    private String muscleGroups;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("added_date")
    @Expose
    private String addedDate;
    @SerializedName("like_count")
    @Expose
    private String likeCount;

    @SerializedName("wo_img_url")
    @Expose
    private String woImageUrl;

    //@SerializedName("fn_level_id")
    @Expose
    private int fitnessLevelId;

    public int getFitnessLevelId() {
        return fitnessLevelId;
    }

    public void setFitnessLevelId(int fitnessLevelId) {
        this.fitnessLevelId = fitnessLevelId;
    }

    public String getWoImageUrl() {
        return woImageUrl;
    }

    public void setWoImageUrl(String woImageUrl) {
        this.woImageUrl = woImageUrl;
    }

    public int getWorkoutId() {
        return Integer.parseInt(workoutId);
    }

    public void setWorkoutId(String workoutId) {
        this.workoutId = workoutId;
    }

    public String getWorkoutCategoryId() {
        return workoutCategoryId;
    }

    public void setWorkoutCategoryId(String workoutCategoryId) {
        this.workoutCategoryId = workoutCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkoutGoal() {
        return workoutGoal;
    }

    public void setWorkoutGoal(String workoutGoal) {
        this.workoutGoal = workoutGoal;
    }

    public String getMuscleGroups() {
        return muscleGroups;
    }

    public void setMuscleGroups(String muscleGroups) {
        this.muscleGroups = muscleGroups;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public int getLikeCount() {
        return Integer.parseInt(likeCount);
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

}
