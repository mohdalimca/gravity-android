
package com.gravityacademy.model.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkoutLibraryDataModel implements Serializable {


    @SerializedName("wo_lib")
    @Expose
    private List<WorkoutLibraryDetailsModel> workoutLibraryDetailsModel = null;

    public List<WorkoutLibraryDetailsModel> getWorkoutLibraryDetailsModel() {
        return workoutLibraryDetailsModel;
    }

    public void setWorkoutLibraryDetailsModel(List<WorkoutLibraryDetailsModel> workoutLibraryDetailsModel) {
        this.workoutLibraryDetailsModel = workoutLibraryDetailsModel;
    }

}
