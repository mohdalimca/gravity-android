
package com.gravityacademy.model.response;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultSet implements Serializable {

    @SerializedName("round_title")
    @Expose
    private String roundTitle;
    @SerializedName("no_of_sets")
    @Expose
    private String noOfSets;
    @SerializedName("exercise")
    @Expose
    private List<Exercise> exercise = null;

    @SerializedName("workouts")
    @Expose
    private List<Workout> workouts = null;

    public List<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(List<Workout> workouts) {
        this.workouts = workouts;
    }

    public String getRoundTitle() {
        return roundTitle;
    }

    public void setRoundTitle(String roundTitle) {
        this.roundTitle = roundTitle;
    }

    public String getNoOfSets() {
        return noOfSets;
    }

    public void setNoOfSets(String noOfSets) {
        this.noOfSets = noOfSets;
    }

    public List<Exercise> getExercise() {
        return exercise;
    }

    public void setExercise(List<Exercise> exercise) {
        this.exercise = exercise;
    }

}
