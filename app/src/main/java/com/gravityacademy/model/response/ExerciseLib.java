
package com.gravityacademy.model.response;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExerciseLib implements Serializable {

    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("total")
    @Expose
    private String total;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}
