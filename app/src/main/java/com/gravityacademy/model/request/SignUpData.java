package com.gravityacademy.model.request;

import java.io.Serializable;

public class SignUpData implements Serializable {
    private String gender;
    private String device_type = "1";
    private String height;
    private String height_unit;
    private String weight;
    private String weight_unit;
    private Integer fn_level_id;
    private String goals;
    private Integer max_pullups;
    private Integer max_pushups;
    private Integer max_squats;
    private Integer max_dips;
    private String user_name;
    private String email;
    private String password;
    private String name;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHeight_unit() {
        return height_unit;
    }

    public void setHeight_unit(String height_unit) {
        this.height_unit = height_unit;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeight_unit() {
        return weight_unit;
    }

    public void setWeight_unit(String weight_unit) {
        this.weight_unit = weight_unit;
    }

    public Integer getFn_level_id() {
        return fn_level_id;
    }

    public void setFn_level_id(Integer fn_level_id) {
        this.fn_level_id = fn_level_id;
    }

    public String getGoals() {
        return goals;
    }

    public void setGoals(String goals) {
        this.goals = goals;
    }

    public Integer getMax_pullups() {
        return max_pullups;
    }

    public void setMax_pullups(Integer max_pullups) {
        this.max_pullups = max_pullups;
    }

    public Integer getMax_pushups() {
        return max_pushups;
    }

    public void setMax_pushups(Integer max_pushups) {
        this.max_pushups = max_pushups;
    }

    public Integer getMax_squats() {
        return max_squats;
    }

    public void setMax_squats(Integer max_squats) {
        this.max_squats = max_squats;
    }

    public Integer getMax_dips() {
        return max_dips;
    }

    public void setMax_dips(Integer max_dips) {
        this.max_dips = max_dips;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
