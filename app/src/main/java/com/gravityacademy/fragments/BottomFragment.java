package com.gravityacademy.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.listeners.BottomOptionClickListener;

import java.util.Objects;


/**
 * Created by sali on 25.December,2018
 */
public class BottomFragment extends Fragment implements View.OnClickListener {


    public CheckBox getCbOffers() {
        cbFavourite.setEnabled(true);
        cbFavourite.setChecked(false);
        return cbFavourite;
    }

    private CheckBox cbWorkout;
    private CheckBox cbFavourite;
    private CheckBox cbTimeline;
    private CheckBox cbProfile;
    public CheckBox cbAcademy;
    public static TextView tvWorkoutLabel;
    public static TextView tvFavoriteLabel;
    public static TextView tvAcademyLabel;
    public static TextView tvProfileLabel;
    public static TextView tvTimelineLabel;
    public FrameLayout viewWorkout;
    public FrameLayout viewFavourite;
    public FrameLayout viewTimeline;
    public FrameLayout viewAcademy;
    public FrameLayout viewProfile;
    public View viewShowAlert;

    private BottomOptionClickListener bottomOptionClickListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bottomOptionClickListener = (BottomOptionClickListener) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_fragment, container, false);

        cbWorkout = view.findViewById(R.id.cbWorkout);
        cbFavourite = view.findViewById(R.id.cbFavourite);
        cbTimeline = view.findViewById(R.id.cbTimeline);
        cbProfile = view.findViewById(R.id.cbProfile);
        cbAcademy = view.findViewById(R.id.cbAcademy);

        tvWorkoutLabel = view.findViewById(R.id.tvWorkoutLabel);
        tvFavoriteLabel = view.findViewById(R.id.tvFavoriteLabel);
        tvProfileLabel = view.findViewById(R.id.tvProfileLabel);
        tvTimelineLabel = view.findViewById(R.id.tvTimelineLabel);
        tvAcademyLabel = view.findViewById(R.id.tvAcademyLabel);

        viewWorkout = view.findViewById(R.id.viewWorkout);
        viewFavourite = view.findViewById(R.id.viewFavourite);
        viewTimeline = view.findViewById(R.id.viewTimeline);
        viewAcademy = view.findViewById(R.id.viewAcademy);
        viewProfile = view.findViewById(R.id.viewProfile);

        viewShowAlert = view.findViewById(R.id.viewShowAlert);
        viewWorkout.setOnClickListener(this);
        viewFavourite.setOnClickListener(this);
        viewTimeline.setOnClickListener(this);
        viewAcademy.setOnClickListener(this);
        viewProfile.setOnClickListener(this);
        return view;
    }

    public FrameLayout getViewHome() {
        return viewWorkout;
    }

    public View getViewBottom() {
        return viewShowAlert;
    }

    public FrameLayout getviewFavourite() {
        return viewFavourite;
    }

    public FrameLayout getViewProfile() {
        return viewProfile;
    }

    private void enableAll() {
        cbWorkout.setEnabled(true);
        cbFavourite.setEnabled(true);
        cbTimeline.setEnabled(true);
        cbProfile.setEnabled(true);
        cbAcademy.setEnabled(true);
    }

    private void setAllFalse() {

        cbWorkout.setChecked(false);
        cbFavourite.setChecked(false);
        cbTimeline.setChecked(false);
        cbProfile.setChecked(false);
        cbAcademy.setChecked(false);
        tvWorkoutLabel.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.white));
        tvFavoriteLabel.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        tvProfileLabel.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        tvTimelineLabel.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        tvAcademyLabel.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));

    }


    @Override
    public void onClick(View v) {
        performOperation(v);
    }

    private void performOperation(View v) {
        setAllFalse();
        switch (v.getId()) {
            case R.id.viewWorkout:
                if (!cbWorkout.isChecked()) {
                    cbWorkout.setEnabled(false);
                    cbWorkout.setChecked(true);
                    tvWorkoutLabel.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.orange));
                    bottomOptionClickListener.onWorkoutButtonClicked();
                }

                break;
            case R.id.viewFavourite:
                if (!cbFavourite.isChecked()) {
                    cbFavourite.setEnabled(false);
                    cbFavourite.setChecked(true);
                    tvFavoriteLabel.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.orange));
                    bottomOptionClickListener.onFavouriteButtonClicked();
                }
                break;
            case R.id.viewTimeline:
                if (!cbTimeline.isChecked()) {
                    cbTimeline.setEnabled(false);
                    cbTimeline.setChecked(true);
                    tvTimelineLabel.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.orange));
                    bottomOptionClickListener.onTimelineButtonClicked();
                }
                break;
            case R.id.viewAcademy:
                if (!cbAcademy.isChecked()) {
                    cbAcademy.setEnabled(false);
                    cbAcademy.setChecked(true);
                    tvAcademyLabel.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.orange));
                    bottomOptionClickListener.onAcademyButtonClicked();
                }
                break;
            case R.id.viewProfile:
                if (!cbProfile.isChecked()) {
                    cbProfile.setEnabled(false);
                    cbProfile.setChecked(true);
                    tvProfileLabel.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.orange));
                    bottomOptionClickListener.onProfileButtonClicked();
                }
                break;
        }
    }

}
