package com.gravityacademy.fragments;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gravityacademy.BuildConfig;
import com.gravityacademy.ProgressView.CircularProgress;
import com.gravityacademy.ProgressView.OnProgressTrackListener;
import com.gravityacademy.R;
import com.gravityacademy.activities.CreateNewWorkoutActivity;
import com.gravityacademy.activities.EditProfileActivity;
import com.gravityacademy.adapters.ProfileAdapter;
import com.gravityacademy.adapters.SkillsAdapter;
import com.gravityacademy.model.CategoryModel;
import com.gravityacademy.utils.AppPermissionManager;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.ImageCompressor;
import com.jackandphantom.circularprogressbar.CircleProgressbar;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnProfileFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ProfileFragment context;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    CircleProgressbar circleProgressbar;
    private AppCompatButton btnEditProfile, btnCreateWorkout;
    private RecyclerView rvTopCategory;
    private OnProfileFragmentInteractionListener mListener;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<CategoryModel> exersiseList;
    private ProfileAdapter mAdapter;
    private RadioButton rdbMuscle, rdbCardio, rdbStrength, rdbTechnique;
    private RadioGroup rdGroup;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    private void setProgress(int per) {
        circleProgressbar.enabledTouch(false);
        circleProgressbar.setRoundedCorner(true);
        circleProgressbar.setClockwise(false);
        int animationDuration = 2500; // 2500ms = 2,5s
        circleProgressbar.setProgressWithAnimation(per, animationDuration); // Default duration = 1500ms
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        circleProgressbar = view.findViewById(R.id.circleProgressbar);
        btnEditProfile = view.findViewById(R.id.btnEditProfile);
        btnCreateWorkout = view.findViewById(R.id.btnCreateWorkout);
        rvTopCategory = view.findViewById(R.id.rvTopCategory);
        rdGroup = view.findViewById(R.id.rdGroup);
        rdGroup.setOnCheckedChangeListener(radioGroupListener);
        btnEditProfile.setOnClickListener(this);
        btnCreateWorkout.setOnClickListener(this);
        prepareList();
        setupRecyclerView();
        setProgress(85);
    }


    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvTopCategory.setLayoutManager(mLinearLayoutManager);
        mAdapter = new ProfileAdapter(getActivity(), exersiseList);
        rvTopCategory.setAdapter(mAdapter);

    }

    private void prepareList() {
        exersiseList = new ArrayList<>();
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.chest));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.mipmap.workout);
        exersiseList.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle(getString(R.string.traisap));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.training);
        exersiseList.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);
        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle(getString(R.string.shoulder));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.mipmap.skills);
        exersiseList.add(categoryModel);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProfileFragmentInteractionListener) {
            mListener = (OnProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnEditProfile:
                startActivity(new Intent(getContext(), EditProfileActivity.class));
                break;
            case R.id.btnCreateWorkout:
                startActivity(new Intent(getContext(), CreateNewWorkoutActivity.class));
                break;
        }


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnProfileFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void onRadioButtonClicked(int id) {
        String str = "";
        // Check which radio button was clicked
        switch (id) {
            case R.id.rdbMuscle:
                str = "Muscle Selected";
                break;
            case R.id.rdbStrength:
                str = "Strength Selected";
                break;
            case R.id.rdbCardio:
                str = "Cardio Selected";
                break;
            case R.id.rdbTechnique:
                str = "Technique Selected";
                break;
        }
        Toast.makeText(getContext(), str, Toast.LENGTH_SHORT).show();
    }

    private RadioGroup.OnCheckedChangeListener radioGroupListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            // find which radio button is selected
            onRadioButtonClicked(checkedId);
        }
    };


}
