package com.gravityacademy.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.gravityacademy.R;
import com.gravityacademy.activities.HomeActivity;
import com.gravityacademy.activities.StrengthSelectActivity;
import com.gravityacademy.activities.StrengthTrainingActivity;
import com.gravityacademy.adapters.ExerciseParentAdapter;
import com.gravityacademy.adapters.HomeFragmentAdapter;
import com.gravityacademy.adapters.MuscleGroupAdapter;
import com.gravityacademy.model.CategoryModel;
import com.gravityacademy.model.response.Workout;
import com.gravityacademy.model.response.WorkoutOfTheDayDataModel;
import com.gravityacademy.model.response.WorkoutOfTheDayModel;
import com.gravityacademy.retrofit.APIUtils;
import com.gravityacademy.utils.AppConstant;
import com.gravityacademy.utils.AppUtils;
import com.gravityacademy.utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gravityacademy.utils.AppConstant.SUCCESS_RESPONSE_CODE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnHomeFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView rvMuscleGroup,rvDashboard;
    private RelativeLayout layoutTop;
    private HomeFragmentAdapter mAdapter;
    private ArrayList<CategoryModel> categoryModels;
    private LinearLayoutManager mLinearLayoutManager;
    private LinearLayoutManager mMuscleGroupLinearLayoutManager;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private LinearLayout layoutProgressBar;
    private TextView tvDate;
    private TextView tvTitle;
    private ImageView ivFavourite;
    private ImageView ivWorkoutBackground;

    private OnHomeFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
    }

    private void initView(View view) {
        rvDashboard = view.findViewById(R.id.rvDashboard);
        rvMuscleGroup = view.findViewById(R.id.rvMuscleGroup);
        layoutTop = view.findViewById(R.id.layoutTop);
        layoutProgressBar = view.findViewById(R.id.layoutProgressBar);
        tvDate=view.findViewById(R.id.tvDate);
        tvTitle=view.findViewById(R.id.tvTitle);
        ivFavourite=view.findViewById(R.id.ivFavourite);
        ivWorkoutBackground=view.findViewById(R.id.ivWorkoutBackground);
        callWorkoutOfTheDayAPI();
        //performLikeDislike(0);

    }

    private void setupRecyclerViewMuscleGroup(String muscleGroup) {
        String [] strings = muscleGroup.split(",");
        List<String> muscleGroups = new ArrayList<>(Arrays.asList(strings)); //new ArrayList is only needed if you absolutely need an ArrayList
        mMuscleGroupLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvMuscleGroup.setLayoutManager(mMuscleGroupLinearLayoutManager);
        MuscleGroupAdapter mAdapter = new MuscleGroupAdapter(getActivity(), muscleGroups,R.layout.item_muscle_group);
        rvMuscleGroup.setAdapter(mAdapter);
    }

    private void setupRecyclerView() {
        mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvDashboard.setLayoutManager(mLinearLayoutManager);
        categoryModels = new ArrayList<>();
        prepairList();
        mAdapter = new HomeFragmentAdapter(getActivity(), categoryModels);
        rvDashboard.setAdapter(mAdapter);
        rvDashboard.setNestedScrollingEnabled(false);
    }

    private void prepairList() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.workouts));
        categoryModel.setTitle(getString(R.string.workouts));
        categoryModel.setSubTitle(getString(R.string.workouts_archiv));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.drawable.type_workout);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.training_plans));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.training_plans));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.drawable.type_training);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.skills));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.handstand));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.drawable.type_skills);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.exercises));
        categoryModel.setTitle("");
        categoryModel.setSubTitle(getString(R.string.calisthenics_weights));
        categoryModel.setCount(12);
        categoryModel.setImageId(R.drawable.type_exercies);
        categoryModels.add(categoryModel);

        categoryModel = new CategoryModel();
        categoryModel.setType(getString(R.string.equipment));
        categoryModel.setTitle(getString(R.string.weeks));
        categoryModel.setSubTitle(getString(R.string.trainingsequipment));
        categoryModel.setCount(0);
        categoryModel.setImageId(R.drawable.type_equipment);
        categoryModels.add(categoryModel);

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHomeFragmentInteractionListener) {
            mListener = (OnHomeFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnHomeFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void performLikeDislike(int value) {
        String tag=value>0?"true":"false";
        ivFavourite.setTag(tag);
        ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ivFavourite.getTag().equals("true")) {
                    ivFavourite.setImageResource(R.mipmap.heart_blank);
                    ivFavourite.setTag("false");
                } else {
                    ivFavourite.setImageResource(R.mipmap.heart_fill);
                    ivFavourite.setTag("true");
                }
            }
        });

    }
    private void setUpWorkOutOfTheDayView(final Workout workout){
        tvDate.setText(AppUtils.changeDateFormat(workout.getAddedDate()));
        tvTitle.setText(workout.getTitle());
        performLikeDislike(workout.getLikeCount());
        setWorkoutOfTheDayBackgroundImage(workout.getWoImageUrl());
        setupRecyclerViewMuscleGroup(workout.getMuscleGroups());
        layoutTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), StrengthSelectActivity.class);
                intent.putExtra(AppConstant.WORKOUT_OBJ,workout);
                startActivity(intent);
            }
        });
    }

    private void setWorkoutOfTheDayBackgroundImage(String url){
        try {
            if (url != null) {
                RequestOptions options = new RequestOptions()
                        .priority(Priority.NORMAL)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);

                Glide.with(Objects.requireNonNull(getContext()))
                        .load(url)
                        .apply(options)
                        .into(ivWorkoutBackground);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void callWorkoutOfTheDayAPI() {
        layoutProgressBar.setVisibility(View.VISIBLE);
        APIUtils.getAPIServiceJson().getWorkoutOfTheDay().enqueue(new Callback<WorkoutOfTheDayModel>() {
            @Override
            public void onResponse(Call<WorkoutOfTheDayModel> call, Response<WorkoutOfTheDayModel> response) {
                if (isVisible()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    if (response != null && response.body() != null) {
                        if (response.body().getResponseCode() == SUCCESS_RESPONSE_CODE) {
                            WorkoutOfTheDayDataModel workoutOfTheDayDataModel = response.body().getData();
                            if(workoutOfTheDayDataModel.getWorkouts()!=null && workoutOfTheDayDataModel.getWorkouts().size()>0){
                                setUpWorkOutOfTheDayView(workoutOfTheDayDataModel.getWorkouts().get(0));
                            }
                        } else {
                            Utility.showToast(getContext(), R.string.something_went_wrong);
                        }
                    } else if (response != null && response.errorBody() != null) {
                        Utility.showToastString(getContext(), AppUtils.getError(response));
                    } else {
                        Utility.showToast(getContext(), R.string.something_went_wrong);
                    }
                }

            }

            @Override
            public void onFailure(Call<WorkoutOfTheDayModel> call, Throwable t) {
                if (isVisible()) {
                    layoutProgressBar.setVisibility(View.GONE);
                    Utility.showToast(getContext(), R.string.something_went_wrong);
                }
            }
        });
    }


}
