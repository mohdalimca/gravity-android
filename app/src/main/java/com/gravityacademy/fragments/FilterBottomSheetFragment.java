package com.gravityacademy.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gravityacademy.R;

import java.util.ArrayList;
import java.util.Objects;

public class FilterBottomSheetFragment extends BottomSheetDialogFragment {
    public static final String TAG = "FilterBottomSheetFragment";
    private TextView tvAllLevel;
    private TextView tvBeginner;
    private TextView tvAdvanced;
    private TextView tvProfessional;
    private TextView tvAllGoals;
    private TextView tvBodyTension;
    private TextView tvFatBurning;
    private TextView tvMuscleBuilding;
    private TextView tvEndurance;
    private TextView tvKraft;
    private TextView tvAllAds;
    private TextView tvFitnessStudio;
    private TextView tvCalisthenicsBars;
    private TextView tvHomeWorkout;
    private ArrayList<String> selectedFilter = new ArrayList<>();

    public static FilterBottomSheetFragment newInstance() {
        return new FilterBottomSheetFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter_bottom_sheet, container, false);
    }

    private void init(View view) {
        tvAllLevel = view.findViewById(R.id.tvAllLevel);
        tvBeginner = view.findViewById(R.id.tvBeginner);
        tvAdvanced = view.findViewById(R.id.tvAdvanced);
        tvProfessional = view.findViewById(R.id.tvProfessional);
        tvAllGoals = view.findViewById(R.id.tvAllGoals);
        tvBodyTension = view.findViewById(R.id.tvBodyTension);
        tvFatBurning = view.findViewById(R.id.tvFatBurning);
        tvMuscleBuilding = view.findViewById(R.id.tvMuscleBuilding);
        tvEndurance = view.findViewById(R.id.tvEndurance);
        tvKraft = view.findViewById(R.id.tvKraft);
        tvAllAds = view.findViewById(R.id.tvAllAds);
        tvFitnessStudio = view.findViewById(R.id.tvFitnessStudio);
        tvCalisthenicsBars = view.findViewById(R.id.tvCalisthenicsBars);
        tvHomeWorkout = view.findViewById(R.id.tvHomeWorkout);
        performClick(tvAllLevel);
        performClick(tvBeginner);
        performClick(tvAdvanced);
        performClick(tvProfessional);
        performClick(tvAllGoals);
        performClick(tvBodyTension);
        performClick(tvFatBurning);
        performClick(tvMuscleBuilding);
        performClick(tvEndurance);
        performClick(tvKraft);
        performClick(tvAllAds);
        performClick(tvFitnessStudio);
        performClick(tvCalisthenicsBars);
        performClick(tvHomeWorkout);

    }

    private void performClick(final TextView textView) {

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tagId = textView.getTag().toString();
                if (tagId.equals("0")) {
                    selectedFilter.add(textView.getText().toString());
                    textView.setTag(String.valueOf(1));
                    textView.setBackgroundTintList(ContextCompat.getColorStateList(Objects.requireNonNull(getContext()), R.color.black));
                    textView.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                } else {
                    String text = textView.getText().toString();
                    selectedFilter.remove(text);
                    textView.setTag(String.valueOf(0));
                    textView.setBackgroundTintList(ContextCompat.getColorStateList(Objects.requireNonNull(getContext()), R.color.light_white));
                    textView.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
                }
            }
        });

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
