package com.gravityacademy.retrofit;

import com.gravityacademy.model.request.SignUpData;
import com.gravityacademy.model.response.SignUpModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIService {


    @POST("user/auth/signup")
    @FormUrlEncoded
    Call<SignUpModel> googleSignUp(@Field("email") String email,
                                   @Field("password") String password,
                                   @Field("user_name") String user_name,
                                   @Field("name") String name,
                                   @Field("gender") String gender,
                                   @Field("device_type") String device_type,
                                   @Field("device_id") String device_id,
                                   @Field("height") String height,
                                   @Field("height_unit") String height_unit,
                                   @Field("weight") String weight,
                                   @Field("weight_unit") String weight_unit,
                                   @Field("fn_level_id") Integer fn_level_id,
                                   @Field("goals") String goals,
                                   @Field("max_pullups") Integer max_pullups,
                                   @Field("max_pushups") Integer max_pushups,
                                   @Field("max_squats") Integer max_squats,
                                   @Field("max_dips") Integer max_dips);

}
