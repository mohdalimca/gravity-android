package com.gravityacademy;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.gravityacademy.fragments.ActiveNowFragment;
import com.gravityacademy.fragments.EBookDetailsFragment;
import com.gravityacademy.fragments.MoreEBookFragment;

public class EBookDetailsActivity extends AppCompatActivity implements
        EBookDetailsFragment.OnFragmentInteractionListener,
        MoreEBookFragment.OnFragmentInteractionListener,
        ActiveNowFragment.OnFragmentInteractionListener{

    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private  TextView tvTitle;;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebook_details);
        initView();
        setDefaultFragment(new EBookDetailsFragment());
    }

    private void initView(){
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("");
        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
   // This method is used to set the default fragment that will be shown.
    private void setDefaultFragment(Fragment defaultFragment) {
        this.replaceFragment(defaultFragment);
    }

    public void replaceFragment(Fragment destFragment) {
        fragmentManager = this.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        Log.e("destFragment: ", destFragment.getClass().getSimpleName());
        fragmentTransaction.replace(R.id.contentLayout, destFragment, destFragment.getClass().getSimpleName());
        if (!(destFragment instanceof EBookDetailsFragment))
            fragmentTransaction.addToBackStack(destFragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(Fragment fragment) {
        replaceFragment(fragment);
    }

    @Override
    public void onUIUpdate(Fragment fragment) {
        if (fragment instanceof MoreEBookFragment){
            tvTitle.setText(getString(R.string.ebook));
        }else {
            tvTitle.setText("");
        }
    }
}
