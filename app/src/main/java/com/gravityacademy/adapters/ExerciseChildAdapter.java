package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.activities.ExerciseDetailActivity;
import com.gravityacademy.model.CategoryModel;
import com.gravityacademy.model.response.Exercise;

import java.util.List;

public class ExerciseChildAdapter extends RecyclerView.Adapter<ExerciseChildAdapter.ViewHolder> {

    private Context mContext;
    private List<Exercise> exercises;
    private boolean isFavouriteIconVisible;

    public ExerciseChildAdapter(Context mContext, List<Exercise> exercises, boolean isFavouriteIconVisible) {
        this.mContext = mContext;
        this.exercises = exercises;
        this.isFavouriteIconVisible = isFavouriteIconVisible;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_library_item, parent, false);

        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Exercise exercise=exercises.get(i);

        if (isFavouriteIconVisible) {
            viewHolder.ivFavourite.setVisibility(View.VISIBLE);
            performLikeDislike(viewHolder.ivFavourite);
        } else {
            viewHolder.ivFavourite.setVisibility(View.INVISIBLE);
        }
        viewHolder.tvTitle.setText(exercise.getTitle());
        viewHolder.tvSubTitle.setText(exercise.getDescription());

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ExerciseDetailActivity.class);
                mContext.startActivity(intent);
            }
        });
       /* if (categoryModels.get(i) != null) {
            final CategoryModel categoryModel = categoryModels.get(i);
            viewHolder.tvTitle.setText(categoryModel.getTitle());
            viewHolder.tvSubTitle.setText(categoryModel.getSubTitle());
            viewHolder.tvType.setText(categoryModel.getType());
            if (categoryModel.getCount() > 0)
                viewHolder.tvCount.setText(String.valueOf(categoryModel.getCount()));
            viewHolder.ivCategoryImage.setImageResource(categoryModel.getImageId());
            viewHolder.tvShowAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent = new Intent(mContext, SeeAllActivity.class);
                    //intent.putExtra(AppConstant.KEY_TYPE, categoryModel.getType());
                    //mContext.startActivity(intent);
                }
            });
        }*/
    }

    @Override
    public int getItemCount() {
        return exercises.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvSubTitle;
        ImageView ivFavourite;
        RelativeLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            ivFavourite = itemView.findViewById(R.id.ivFavourite);
        }
    }

    private void performLikeDislike(final ImageView ivFavourite) {
       // final ImageView ivFavourite = findViewById(R.id.ivFavourite);
        ivFavourite.setTag("false");
        ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ivFavourite.getTag().equals("true")) {
                    ivFavourite.setImageResource(R.mipmap.heart_blank);
                    ivFavourite.setTag("false");
                } else {
                    ivFavourite.setImageResource(R.mipmap.heart_fill);
                    ivFavourite.setTag("true");
                }
            }
        });

    }

}
