package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.activities.ProgressionProgramScreenActivity;
import com.gravityacademy.activities.SkillsActivity;
import com.gravityacademy.model.CategoryModel;

import java.util.List;

public class ProgramAdapter extends RecyclerView.Adapter<ProgramAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryModel> categoryModels;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout layoutProgressBar;

    public ProgramAdapter(Activity mContext, List<CategoryModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_program, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (categoryModels.get(i) != null) {

        }
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProgressionProgramScreenActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvShowAll;
        TextView tvTitle;
        TextView tvSubTitle;
        TextView tvCount;
        TextView tvType;
        ImageView ivCategoryImage;
        LinearLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            /*tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvShowAll = itemView.findViewById(R.id.tvShowAll);
            tvType = itemView.findViewById(R.id.tvType);
            ivCategoryImage = itemView.findViewById(R.id.ivCategoryImage);*/
        }
    }
}
