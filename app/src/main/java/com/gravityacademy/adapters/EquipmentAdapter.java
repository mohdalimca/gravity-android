package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.activities.ProgramScreenActivity;
import com.gravityacademy.activities.WebViewActivity;
import com.gravityacademy.model.CategoryModel;

import java.util.List;

public class EquipmentAdapter extends RecyclerView.Adapter<EquipmentAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryModel> categoryModels;

    public EquipmentAdapter(Activity mContext, List<CategoryModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_equipment_item, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
       /* if (categoryModels.get(i) != null) {
            final CategoryModel categoryModel = categoryModels.get(i);
            viewHolder.tvTitle.setText(categoryModel.getTitle());
            viewHolder.tvSubTitle.setText(categoryModel.getSubTitle());
            viewHolder.tvType.setText(categoryModel.getType());
            if (categoryModel.getCount() > 0)
                viewHolder.tvCount.setText(String.valueOf(categoryModel.getCount()));
            viewHolder.ivCategoryImage.setImageResource(categoryModel.getImageId());
            viewHolder.tvShowAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent = new Intent(mContext, SeeAllActivity.class);
                    //intent.putExtra(AppConstant.KEY_TYPE, categoryModel.getType());
                    //mContext.startActivity(intent);
                }
            });
        }*/

       viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(mContext, WebViewActivity.class);
               mContext.startActivity(intent);
           }
       });
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        LinearLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }
}
