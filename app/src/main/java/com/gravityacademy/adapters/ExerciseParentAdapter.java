package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.response.Exercise;
import com.gravityacademy.model.response.ResultSet;
import com.gravityacademy.model.response.WorkoutLibraryDetailsModel;
import com.gravityacademy.model.response.Workout;

import java.util.List;

public class ExerciseParentAdapter extends RecyclerView.Adapter<ExerciseParentAdapter.ViewHolder> {

    private Context mContext;
    private List<ResultSet> resultSets;

    public ExerciseParentAdapter(Activity mContext, List<ResultSet> workoutLibraryDetailsModels) {
        this.mContext = mContext;
        this.resultSets = workoutLibraryDetailsModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exercise_child, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ResultSet resultSet = resultSets.get(i);
        viewHolder.tvTitle.setText(resultSet.getRoundTitle());
        viewHolder.tvSubTitle.setText(String.valueOf(resultSet.getNoOfSets()));
        setupRecyclerView(viewHolder.rvExerciseChild, resultSet.getExercise());

       /* if (categoryModels.get(i) != null) {
            final CategoryModel categoryModel = categoryModels.get(i);
            viewHolder.tvTitle.setText(categoryModel.getTitle());
            viewHolder.tvSubTitle.setText(categoryModel.getSubTitle());
            viewHolder.tvType.setText(categoryModel.getType());
            if (categoryModel.getCount() > 0)
                viewHolder.tvCount.setText(String.valueOf(categoryModel.getCount()));
            viewHolder.ivCategoryImage.setImageResource(categoryModel.getImageId());
            viewHolder.tvShowAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent = new Intent(mContext, SeeAllActivity.class);
                    //intent.putExtra(AppConstant.KEY_TYPE, categoryModel.getType());
                    //mContext.startActivity(intent);
                }
            });
        }*/

    }

    @Override
    public int getItemCount() {
        return resultSets.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvSubTitle;
        RecyclerView rvExerciseChild;

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle);
            rvExerciseChild = itemView.findViewById(R.id.rvExerciseChild);
        }
    }

    private void setupRecyclerView(RecyclerView rvWorkoutLibraryHorizontal, List<Exercise> exercises) {
        if (exercises != null && exercises.size() > 0) {
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rvWorkoutLibraryHorizontal.setLayoutManager(mLinearLayoutManager);
            ExerciseChildAdapter mAdapter = new ExerciseChildAdapter(mContext, exercises,false);
            rvWorkoutLibraryHorizontal.setAdapter(mAdapter);
            rvWorkoutLibraryHorizontal.setNestedScrollingEnabled(false);
        }
    }

}
