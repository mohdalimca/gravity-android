package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.activities.ShowAllFavouriteActivity;
import com.gravityacademy.model.CategoryModel;
import com.gravityacademy.model.response.Exercise;
import com.gravityacademy.model.response.ResultSet;
import com.gravityacademy.model.response.WorkoutLibraryDetailsModel;
import com.gravityacademy.model.response.Workout;
import com.gravityacademy.utils.AppConstant;

import java.util.List;

public class FavouriteVerticalAdapter extends RecyclerView.Adapter<FavouriteVerticalAdapter.ViewHolder> {

    private Context mContext;
    private List<ResultSet> categoryModels;

    public FavouriteVerticalAdapter(Activity mContext, List<ResultSet> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_workout_library_vertical_item, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ResultSet categoryModel = categoryModels.get(i);
        setupRecyclerView(viewHolder.rvWorkoutLibraryHorizontal, categoryModel.getWorkouts());

        if (categoryModels.get(i) != null) {
            viewHolder.tvTitle.setText(categoryModel.getRoundTitle());
            if (categoryModel.getWorkouts().size() > 0)
            viewHolder.tvShowAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ShowAllFavouriteActivity.class);
                    mContext.startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvShowAll;
        TextView tvTitle;
        RecyclerView rvWorkoutLibraryHorizontal;

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            rvWorkoutLibraryHorizontal = itemView.findViewById(R.id.rvWorkoutLibraryHorizontal);
            tvShowAll = itemView.findViewById(R.id.tvShowAll);
        }
    }

    private void setupRecyclerView(RecyclerView rvWorkoutLibraryHorizontal, List<Workout> workouts) {
        if (workouts != null && workouts.size() > 0) {
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            rvWorkoutLibraryHorizontal.setLayoutManager(mLinearLayoutManager);
            FavouriteHorizontalAdapter mAdapter = new FavouriteHorizontalAdapter(mContext, workouts);
            rvWorkoutLibraryHorizontal.setAdapter(mAdapter);
            rvWorkoutLibraryHorizontal.setNestedScrollingEnabled(false);
        }
    }

}
