package com.gravityacademy.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.activities.StrengthTrainingActivity;
import com.gravityacademy.model.CategoryModel;

import java.util.List;

public class ProgressionProgramAdapter extends RecyclerView.Adapter<ProgressionProgramAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryModel> categoryModels;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout layoutProgressBar;

    public ProgressionProgramAdapter(Activity mContext, List<CategoryModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_progression_program, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (categoryModels.get(i) != null) {

        }

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, StrengthTrainingActivity.class));
                //showNoTrainingDialog();
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvShowAll;
        TextView tvTitle;
        TextView tvSubTitle;
        TextView tvCount;
        TextView tvType;
        ImageView ivCategoryImage;
        LinearLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            /*tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvShowAll = itemView.findViewById(R.id.tvShowAll);
            tvType = itemView.findViewById(R.id.tvType);
            ivCategoryImage = itemView.findViewById(R.id.ivCategoryImage);*/
        }
    }

    private void showNoTrainingDialog() {
        final Dialog dialog = new Dialog(mContext, R.style.DialogCustomTheme_);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_no_training_alert);
        Button tvPositive = dialog.findViewById(R.id.btnOk);
        tvPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
