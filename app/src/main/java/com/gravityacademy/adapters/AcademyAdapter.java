package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gravityacademy.EBookDetailsActivity;
import com.gravityacademy.R;
import com.gravityacademy.activities.AcademyActivity;
import com.gravityacademy.activities.AllExerciseActivity;
import com.gravityacademy.activities.ProgramScreenActivity;
import com.gravityacademy.activities.ProgressionProgramScreenActivity;
import com.gravityacademy.model.CategoryModel;

import java.util.List;

public class AcademyAdapter extends RecyclerView.Adapter<AcademyAdapter.ViewHolder> {

    private Context mContext;
    private List<CategoryModel> categoryModels;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout layoutProgressBar;

    public AcademyAdapter(Activity mContext, List<CategoryModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_academy, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        if (categoryModels.get(i) != null) {
            viewHolder.tvTitle.setText(categoryModels.get(i).getTitle());
            viewHolder.ivFirst.setImageResource(categoryModels.get(i).getImageId());
            viewHolder.ivSecond.setImageResource(categoryModels.get(i).getImageId2());
        }
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=null;
                if(i<2){
                    intent = new Intent(mContext, ProgressionProgramScreenActivity.class);
                }else {
                    intent = new Intent(mContext, AllExerciseActivity.class);
                }
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        ImageView ivFirst;
        ImageView ivSecond;
        LinearLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            ivFirst = itemView.findViewById(R.id.ivFirst);
            ivSecond = itemView.findViewById(R.id.ivSecond);
        }
    }
}
