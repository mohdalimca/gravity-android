package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.model.response.WorkoutLibraryDetailsModel;
import com.gravityacademy.model.response.Workout;

import java.util.ArrayList;
import java.util.List;

public class WorkoutLibraryVerticalAdapter extends RecyclerView.Adapter<WorkoutLibraryVerticalAdapter.ViewHolder> {

    private Context mContext;
    private List<WorkoutLibraryDetailsModel> categoryModels;

    public WorkoutLibraryVerticalAdapter(Activity mContext, List<WorkoutLibraryDetailsModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_workout_library_vertical_item, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        WorkoutLibraryDetailsModel workoutLibraryDetailsModel = categoryModels.get(i);
        viewHolder.tvTitle.setText(workoutLibraryDetailsModel.getTitle());
        setupRecyclerView(viewHolder.rvWorkoutLibraryHorizontal, workoutLibraryDetailsModel.getWorkouts(), viewHolder.tvShowAll);

       /* if (categoryModels.get(i) != null) {
            final CategoryModel categoryModel = categoryModels.get(i);
            viewHolder.tvTitle.setText(categoryModel.getTitle());
            viewHolder.tvSubTitle.setText(categoryModel.getSubTitle());
            viewHolder.tvType.setText(categoryModel.getType());
            if (categoryModel.getCount() > 0)
                viewHolder.tvCount.setText(String.valueOf(categoryModel.getCount()));
            viewHolder.ivCategoryImage.setImageResource(categoryModel.getImageId());
            viewHolder.tvShowAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent = new Intent(mContext, SeeAllActivity.class);
                    //intent.putExtra(AppConstant.KEY_TYPE, categoryModel.getType());
                    //mContext.startActivity(intent);
                }
            });
        }*/

    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvShowAll;
        TextView tvTitle;
        RecyclerView rvWorkoutLibraryHorizontal;

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            rvWorkoutLibraryHorizontal = itemView.findViewById(R.id.rvWorkoutLibraryHorizontal);
            tvShowAll = itemView.findViewById(R.id.tvShowAll);
        }
    }

    private void setupRecyclerView(RecyclerView rvWorkoutLibraryHorizontal, List<Workout> workouts, TextView tvShowAll) {
        List<Workout> workoutsTemp = new ArrayList<>();
        if (workouts.size() > 3) {
            tvShowAll.setVisibility(View.VISIBLE);
            for (int i = 0; i < 3; i++) {
                workoutsTemp.add(workouts.get(i));
            }
        } else {
            tvShowAll.setVisibility(View.INVISIBLE);
            workoutsTemp.addAll(workouts);
        }
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvWorkoutLibraryHorizontal.setLayoutManager(mLinearLayoutManager);
        WorkoutLibraryHorizontalAdapter mAdapter = new WorkoutLibraryHorizontalAdapter(mContext, workoutsTemp);
        rvWorkoutLibraryHorizontal.setAdapter(mAdapter);
        rvWorkoutLibraryHorizontal.setNestedScrollingEnabled(false);
    }

}
