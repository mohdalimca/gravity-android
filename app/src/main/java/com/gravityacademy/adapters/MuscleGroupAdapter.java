package com.gravityacademy.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gravityacademy.R;
import com.gravityacademy.activities.WebViewActivity;
import com.gravityacademy.model.CategoryModel;

import java.util.List;

public class MuscleGroupAdapter extends RecyclerView.Adapter<MuscleGroupAdapter.ViewHolder> {

    private Context mContext;
    private List<String> muscles;
    private int resource;

    public MuscleGroupAdapter(Context mContext, List<String> categoryModels,int layout) {
        this.mContext = mContext;
        this.muscles = categoryModels;
        this.resource=layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.tvTitle.setText(muscles.get(i));

       viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(mContext, WebViewActivity.class);
               mContext.startActivity(intent);
           }
       });
    }

    @Override
    public int getItemCount() {
        return muscles.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        RelativeLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }
}
